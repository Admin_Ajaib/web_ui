import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

if (Server.toLowerCase() == 'uat') {
	UI.RunBrowser(GlobalVariable.URL)
	
} else if (Server.toLowerCase() == 'dev') {
	UI.RunBrowser(GlobalVariable.URLdev)
	
} else if (Server.toLowerCase() == 'smoke') {
	UI.RunBrowser(GlobalVariable.URLSmoke)
	
}

WebUI.click(findTestObject('Object Repository/Login/Web/Button_Masuk'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Field_Email'), 30)

WebUI.setText(findTestObject('Object Repository/Register/Field_Email'), Email)

WebUI.setText(findTestObject('Object Repository/Register/Field_Password'), Password)

UI.ScreenShot("Login")

WebUI.click(findTestObject('Object Repository/Register/Button_Daftar'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Field_EnterPIN', [('div') : '1']), 30)

for (int i = 1 ; i <= PIN.size() ; i++) {
	int count = i - 1
	
	if (i == 3) {
		UI.ScreenShot("Enter-PIN")
	}
	
	WebUI.setText(findTestObject('Object Repository/Activation/Field_EnterPIN', [('div') : i]), PIN[count])
}

UI.ScreenShot("Dashboard")

UI.GlobalVar("email", Email)





