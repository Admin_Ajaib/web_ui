import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.WEB

WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']), 30)

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']))

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Watchlist', [('name') : watchlist] ))

WEB.ScreenShot("Watchlist")

ArrayList getStockList0 = WEB.getStockList(findTestObject('Object Repository/Watchlist/All_Watchlist'))

if (getStockList0.size() < 1) {
	KeywordUtil.markFailedAndStop("No stock is added to this watchlist")
}

ArrayList getStockList1 = WEB.getStockList(findTestObject('Object Repository/Watchlist/All_Watchlist'))

boolean exist = WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_SortWatchlist', [('button'):'[contains(@src,"c24d85b")]']), 1, FailureHandling.OPTIONAL)

while (!exist) {
	WebUI.click(findTestObject('Object Repository/Watchlist/Button_SortWatchlist', [('button'):'']))
	
	exist = WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_SortWatchlist', [('button'):'[contains(@src,"c24d85b")]']), 1, FailureHandling.OPTIONAL)
}

WebUI.click(findTestObject('Object Repository/Watchlist/Button_SortWatchlist', [('button'):'[contains(@src,"c24d85b")]']))

WEB.ScreenShot("Ascending")

/** Check Ascending **/
ArrayList getStockList = WEB.getStockList(findTestObject('Object Repository/Watchlist/All_Watchlist'))

Collections.sort(getStockList1)

int count = 0

for (int i = 0 ; i < getStockList.size() ; i++) {
	if (getStockList[i] != getStockList1[i]) {
		count += 1
	}
	
	if (count > 0) {
		WEB.Note(getStockList)
		WEB.Note(getStockList1)
		KeywordUtil.markFailedAndStop("Stocks aren't sorted Ascending")
	} else {
		WEB.Note("Stocks successfully sorted Ascending")
	}
}

WebUI.click(findTestObject('Object Repository/Watchlist/Button_SortWatchlist', [('button'):'[contains(@src,"e37409c")]']))

WEB.ScreenShot("Descending")

/** Check Descending **/
getStockList = WEB.getStockList(findTestObject('Object Repository/Watchlist/All_Watchlist'))

Collections.sort(getStockList1, Collections.reverseOrder())

for (int x = 0 ; x < getStockList.size() ; x++) {
	if (getStockList[x] != getStockList1[x]) {
		count += 1
	}
	
	if (count > 0) {
		WEB.Note(getStockList)
		WEB.Note(getStockList1)
		KeywordUtil.markFailedAndStop("Stocks aren't sorted Descending")
	} else {
		WEB.Note("Stocks successfully sorted Descending")
	}
}

WebUI.click(findTestObject('Object Repository/Watchlist/Button_SortWatchlist', [('button'):'[contains(@src,"d18cfdd")]']))

WEB.ScreenShot("Normal")

/** Check Ascending **/
getStockList = WEB.getStockList(findTestObject('Object Repository/Watchlist/All_Watchlist'))

for (int f = 0 ; f < getStockList.size() ; f++) {
	if (getStockList0[f] != getStockList[f]) {
		count += 1
	}
	
	if (count > 0) {
		WEB.Note(getStockList0)
		WEB.Note(getStockList)
		KeywordUtil.markFailedAndStop("Stocks aren't back to normal sort")
	} else {
		WEB.Note("Stocks successfully back to normal")
	}
}














