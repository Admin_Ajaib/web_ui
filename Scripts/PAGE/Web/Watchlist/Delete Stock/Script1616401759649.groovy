import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.WEB

WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '1']), 30)

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']))

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Watchlist', [('name') : watchlist] ))

WEB.ScreenShot("Watchlist")

ArrayList getStockList = WEB.getStockList(findTestObject('Object Repository/Watchlist/All_Watchlist'))

if (getStockList.size() < 1) {
	KeywordUtil.markFailedAndStop("No stock is added to this watchlist")
}

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '1']))

WebUI.click(findTestObject('Object Repository/Watchlist/Button_GearEdit'))

WEB.ScreenShot("Button Delete")

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Remove Stocks'))

WEB.ScreenShot("Delete")

for (int i = 0 ; i < stockList.size() ; i++) {
	if (!(getStockList.contains(stockList[i]))) {
		KeywordUtil.markFailedAndStop("Can't delete stock due to stock not exist")
	}
	
	WebUI.click(findTestObject('Object Repository/Watchlist/Checkbox_Remove Stocks', [('stock') : stockList[i]]))
}

WEB.ScreenShot("Ready to Delete")

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Popup', [('button') : 'Hapus']))

WEB.ScreenShot("Pop up Delete")

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Popup2', [('button') : 'Hapus']))

WebUI.delay(1)

ArrayList getStockList1 = WEB.getStockList(findTestObject('Object Repository/Watchlist/All_Watchlist'))

println (getStockList1)
println (stockList)

for (int f = 0 ; f < stockList.size ; f++) {
	if (getStockList1.contains(stockList[f])) {
		KeywordUtil.markFailedAndStop(stockList[f] + " should be not exist!")
	}
}


