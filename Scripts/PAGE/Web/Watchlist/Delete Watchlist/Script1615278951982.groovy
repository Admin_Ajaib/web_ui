import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.WEB

WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']), 30)

boolean ButtonWatchlist = WebUI.waitForElementPresent(findTestObject('Object Repository/Watchlist/Button_CreateWatchlist'),
	2, FailureHandling.OPTIONAL)

if (ButtonWatchlist) {
	WebUI.mouseOver(findTestObject('Object Repository/Watchlist/Button_Watchlist', [('name') : watchlist]))
	
} else {
	WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']))

	WebUI.mouseOver(findTestObject('Object Repository/Watchlist/Button_Watchlist', [('name') : watchlist]))
}

WEB.ScreenShot("Hover Watchlist")

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Delete'))

WEB.ScreenShot("Pop Up Delete")

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Popup', [('button') : 'Hapus']))

ArrayList allWatchlist = WEB.getWatchlist(findTestObject('Object Repository/Watchlist/Container_Watchlist'))

WEB.Note(allWatchlist)

WEB.ScreenShot("Watchlist")

if (allWatchlist.contains(watchlist)) {
	KeywordUtil.markFailed("Watchlist " + watchlist + " should not exist.")
	
} else {
	
	WEB.Note("Passed, Watchlist " + watchlist + " is not exist.")
	
}








