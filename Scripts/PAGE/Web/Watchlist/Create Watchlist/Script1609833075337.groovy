import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI as UI
import org.openqa.selenium.Keys as Keys

WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']), 30)

boolean ButtonWatchlist = WebUI.waitForElementPresent(findTestObject('Object Repository/Watchlist/Button_CreateWatchlist'), 
    2, FailureHandling.OPTIONAL)

if (ButtonWatchlist) {
    WebUI.click(findTestObject('Object Repository/Watchlist/Button_CreateWatchlist'))
} else {
    WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']))
	
	UI.ScreenShot("Watchlist")

    WebUI.click(findTestObject('Object Repository/Watchlist/Button_CreateWatchlist'))
}

WebUI.setText(findTestObject('Object Repository/Watchlist/Input_WatchlistName'), WatchlistName)

UI.ScreenShot("Create Watchlist")

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Buat'))

boolean subtitle = WebUI.waitForElementPresent(findTestObject('Object Repository/Watchlist/Text_Subtitle', [('name') : WatchlistName]), 
    2, FailureHandling.OPTIONAL)

if (subtitle) {
    WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']))
} else {
    WebUI.click(findTestObject('Object Repository/Watchlist/Button_ChooseWatchlist', [('name') : WatchlistName]))
}

UI.ScreenShot("Open Watchlist")

for (int i = 0; i < SahamList.size(); i++) {
    WebUI.setText(findTestObject('Object Repository/Watchlist/Search_Saham'), SahamList[i])

    WebUI.sendKeys(findTestObject('Object Repository/Watchlist/Search_Saham'), Keys.chord(Keys.ENTER))
	
	UI.HoverItem(findTestObject('Object Repository/Watchlist/Button_ResultSaham', [('saham') : SahamList[i]]))
	
	int count = i + 1
	
	UI.ScreenShot("Hover Stock " + count)
	
	boolean alreadySaved = WebUI.waitForElementPresent(findTestObject('Object Repository/Watchlist/Button_SaveToWatchUI.ScreenShot("Login")list', [('code') : '61ccfc5']), 
    1, FailureHandling.OPTIONAL)
	
	if (alreadySaved) {
		WebUI.click(findTestObject('Object Repository/Watchlist/button_CancelSearch'))
		
	} else {
		WebUI.click(findTestObject('Object Repository/Watchlist/Button_SaveToWatchlist', [('code') : '8eef179']))
		
		WebUI.click(findTestObject('Object Repository/Watchlist/Search_Saham'))
		
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/Watchlist/button_CancelSearch'))
		
		UI.ScreenShot("StockList")
	}
}

