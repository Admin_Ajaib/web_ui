import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.WEB
import com.keyword.DragAndDropHelper as dnd

WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '1']), 30)

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']))

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Watchlist', [('name') : watchlist] ))

WEB.ScreenShot("Watchlist")

ArrayList getStockList = WEB.getStockList(findTestObject('Object Repository/Watchlist/All_Watchlist'))

if (getStockList.size() < 1) {
	KeywordUtil.markFailedAndStop("No stock is added to this watchlist")
} 

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '1']))

ArrayList stock = new ArrayList()

ArrayList action = new ArrayList()

for (int i = 0 ; i < StockAction.size() ; i++) {
	
	stock.add((StockAction[i].split('-'))[0])
	
	action.add((StockAction[i].split('-'))[1])
}

for (int a = 0 ; a < stock.size() ; a++) {
	if (!(getStockList.contains(stock[a]))) {
		KeywordUtil.markFailedAndStop("Stock " + stock[a] + " not available in watchlist")
	}
}

WEB.ScreenShot("Stocks Before")

switch (type) {
	case 'upDown' :
		for (int y = 0 ; y < stock.size() ; y++) {
			if (action[y].toUpperCase() == 'UP') {
				boolean exist = WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_upDownSort',
					[('stock') : stock[y], 
						('action') : '/0837bf0' ]), 1, FailureHandling.OPTIONAL)
				
				if (exist) {
					WebUI.click(findTestObject('Object Repository/Watchlist/Button_upDownSort',
						[('stock') : stock[y], 
							('action') : '/0837bf0' ]))
				} else {
					KeywordUtil.markWarning("Button UP is not available for this stock")
				}
			} else {
				boolean exist = WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_upDownSort',
					[('stock') : stock[y],
						('action') : '/737d275' ]), 1, FailureHandling.OPTIONAL)
				
				if (exist) {
					WebUI.click(findTestObject('Object Repository/Watchlist/Button_upDownSort',
						[('stock') : stock[y],
							('action') : '/737d275' ]))
				} else {
					KeywordUtil.markWarning("Button DOWN is not available for this stock")
				}
			}
		}
		
		WEB.ScreenShot("Stocks After")
	
		break
		
	case 'Move' :
		for (int v = 0 ; v < action.size() ; v++) {
			if (!(getStockList.contains(action[v]))) {
				KeywordUtil.markFailedAndStop("Stock " + action[v] + " not available in watchlist")
			}
		}
		
		for (int s = 0 ; s < stock.size() ; s++) {
			WEB.dragNdrop(findTestObject('Object Repository/Watchlist/Button_upDownSort',
						[('stock') : stock[s],
							('action') : '/c87046f' ]), 
						findTestObject('Object Repository/Watchlist/Button_upDownSort',
							[('stock') : action[s],
								('action') : '/c87046f' ]))
		}		
		
		WEB.ScreenShot("Stocks After")
		
		break
}

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Popup', [('button') : 'Simpan']))

WEB.ScreenShot("Finished")

















