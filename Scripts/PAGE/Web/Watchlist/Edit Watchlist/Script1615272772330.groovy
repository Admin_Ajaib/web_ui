import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.WEB

WebUI.waitForElementVisible(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']), 30)

boolean ButtonWatchlist = WebUI.waitForElementPresent(findTestObject('Object Repository/Watchlist/Button_CreateWatchlist'),
	2, FailureHandling.OPTIONAL)

if (ButtonWatchlist) {
	WebUI.mouseOver(findTestObject('Object Repository/Watchlist/Button_Watchlist', [('name') : watchlist]))
	
} else {
	WebUI.click(findTestObject('Object Repository/Watchlist/Button_Collapse', [('button') : '2']))

	WebUI.mouseOver(findTestObject('Object Repository/Watchlist/Button_Watchlist', [('name') : watchlist]))
}

WEB.ScreenShot("Hover Watchlist")

WebUI.click(findTestObject('Object Repository/Watchlist/Button_edit'))

WEB.ScreenShot("Edit Watchlist")

WebUI.clearText(findTestObject('Object Repository/Watchlist/Input_WatchlistName'))

WebUI.setText(findTestObject('Object Repository/Watchlist/Input_WatchlistName'), newName)

WEB.ScreenShot("After Edit Watchlist")

WebUI.click(findTestObject('Object Repository/Watchlist/Button_Buat'))

WebUI.delay(1)

ArrayList allWatchlist = WEB.getWatchlist(findTestObject('Object Repository/Watchlist/Container_Watchlist'))

WEB.Note(allWatchlist)

WEB.ScreenShot("Watchlist")

if (allWatchlist.contains(watchlist)) {
	KeywordUtil.markFailed("Watchlist " + watchlist + " should be not exist.")
} else {
	WEB.Note("Passed, Watchlist " + watchlist + " is not exist.")
}

if (allWatchlist.contains(newName)) {
	WEB.Note("Passed, Watchlist " + newName + " is exist.")
	
} else {
	KeywordUtil.markFailed("Watchlist " + newName + " should be exist.")
	
}










