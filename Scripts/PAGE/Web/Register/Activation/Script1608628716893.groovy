import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI as UI
import org.openqa.selenium.Keys as Keys

/** Isi Data Diri **/
boolean stockExist = WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Button_Random'), 10)

if (stockExist) {
	WebUI.click(findTestObject('Object Repository/Activation/Button_Random'))
	
	WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Button_Activation'), 30)
	
	UI.ScreenShot("Button-Activation")
	
	WebUI.click(findTestObject('Object Repository/Activation/Button_Activation'))
	
} else {
	KeywordUtil.markWarning("Stock list on Dashboard is Empty")
	
	UI.HoverItem(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Transaksi']))
	
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_SubHeader', [('menu') : 'Transaksi' , ('submenu') : 'Reksa Dana']))
	
	WebUI.click(findTestObject('Object Repository/Activation/Button_ActivateNowPortofolio'))
}

WebUI.waitForElementClickable(findTestObject('Object Repository/Activation/Field_NIK'), 10)

WebUI.sendKeys(findTestObject('Object Repository/Activation/Field_NIK'), '3')

for (int i = 0; i < NIK.size(); i++) {
    WebUI.sendKeys(findTestObject('Object Repository/Activation/Field_NIK'), NIK[i])
}

UI.GlobalVar("tempnik", UI.getNik() + 1)

UI.updateGlobal("nik", UI.getNik())

WebUI.click(findTestObject('Object Repository/Activation/Button_Upload'))

UI.UploadFile2(FileUpload)

boolean nameExist = WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'Nama']), 1, FailureHandling.OPTIONAL)

if (!nameExist) {
	WebUI.click(findTestObject('Object Repository/Activation/Combobox_Education'))
	
	WebUI.click(findTestObject('Object Repository/Activation/Option_Education', [('edu') : education]))
	
	WebUI.delay(1)
	
	UI.ScreenShot("Step-Data-Diri")
	
	WebUI.click(findTestObject('Object Repository/Activation/Button_Lanjut'))
	
	boolean exist = WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Button_NotFound'), 30, FailureHandling.OPTIONAL)
	
	if (exist) {
		UI.ScreenShot("Data Diri Not Found")
		
		WebUI.click(findTestObject('Object Repository/Activation/Button_NotFound'))
	}
}

WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'Nama']), Nama)

WebUI.click(findTestObject('Object Repository/Activation/Field_TanggalLahir', [('box') : 'Tanggal Lahir']))

UI.datePicker('15 October 2004')

UI.ScreenShot("Data-Diri-1")

WebUI.click(findTestObject('Object Repository/Activation/Field_OpenTanggalLahir'))

WebUI.setText(findTestObject('Object Repository/Activation/Field_TanggalLahir', [('box') : 'Tempat Lahir']), TempatLahir)

WebUI.sendKeys(findTestObject('Object Repository/Activation/Field_TanggalLahir', [('box') : 'Tempat Lahir']), Keys.chord(
        Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Activation/Field_alamat'), Alamat)

UI.ScreenShot("Data-Diri-2")

WebUI.click(findTestObject('Object Repository/Activation/Field_OpenKota'))

WebUI.setText(findTestObject('Object Repository/Activation/Field_TanggalLahir', [('box') : 'Kota']), Kota)

WebUI.sendKeys(findTestObject('Object Repository/Activation/Field_TanggalLahir', [('box') : 'Kota']), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'Kode Pos']), KodePos)

UI.ScreenShot("Data-Diri-3")

if (nameExist) {
	WebUI.click(findTestObject('Object Repository/Activation/Combobox_Education'))
	
	WebUI.click(findTestObject('Object Repository/Activation/Option_Education', [('edu') : education]))
}

WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'Nama Ibu Kandung']), IbuKandung)

WebUI.scrollToElement(findTestObject('Object Repository/Activation/Button_Lanjut'), 1)

UI.ScreenShot("Data-Diri-4")

WebUI.click(findTestObject('Object Repository/Activation/Button_Lanjut'))

/** Profil Risiko **/
UI.ComboBoxOption(findTestObject('Object Repository/Activation/Combo_TotalPenghasilan'), TotalIncome)

UI.ComboBoxOption(findTestObject('Object Repository/Activation/Combo_ToleransiRisiko'), Toleransi)

UI.ComboBoxOption(findTestObject('Object Repository/Activation/Combo_pengalaman'), Pengalaman)

UI.ComboBoxOption(findTestObject('Object Repository/Activation/Combo_JangkaWaktu'), JangkaWaktu)

WebUI.delay(1)

UI.ScreenShot("Profil-Risiko")

WebUI.click(findTestObject('Object Repository/Activation/Button_Lanjut'))

/** Informasi Rekening **/
WebUI.click(findTestObject('Object Repository/Activation/Combo_OpenBank'))

WebUI.setText(findTestObject('Object Repository/Activation/Field_TanggalLahir', [('box') : 'Nama Bank']), Bank)

WebUI.sendKeys(findTestObject('Object Repository/Activation/Field_TanggalLahir', [('box') : 'Nama Bank']), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'Nomor Rekening']), Rekening)

UI.ScreenShot("Informasi-Rekening")

WebUI.click(findTestObject('Object Repository/Activation/Button_Lanjut'))

if (Bank.toLowerCase() == 'bca') {
	boolean RekeningNotFound = WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/PopUp - Rekening'), 5, FailureHandling.OPTIONAL)
	
	def BCARDN = findTestData("BCA RDN").getAllData()
	
	if (RekeningNotFound) {
		if (BCARDN[0].contains(Rekening)) {
			int getIndex = BCARDN[0].indexOf(Rekening)
			
			if (Nama == BCARDN[1][getIndex] && Rekening == BCARDN[0][getIndex]) {
				KeywordUtil.markFailedAndStop("Pop Up Rekening not found should be Not Displayed")
			} else {
				KeywordUtil.markPassed("Correct.. Pop up Rekening not found is Displayed")
				
				UI.forceStop("Test Script Passed - Force Stop")
			}
		} else {
			KeywordUtil.markPassed("Correct.. Pop up Rekening not found is Displayed")
				
			UI.forceStop("Test Script Passed - Force Stop")
		}
	}
}


/** Informasi Pekerjaan **/ 
UI.ComboBoxOption(findTestObject('Object Repository/Activation/Combobox_Universal', [('menu'):'Pekerjaan']), Pekerjaan)

WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'Nama Perusahaan']), Perusahaan)

UI.ComboBoxOption(findTestObject('Object Repository/Activation/Combobox_Universal', [('menu'):'Bidang Pekerjaan']), Bidang)

WebUI.setText(findTestObject('Object Repository/Activation/Field_AlamatKantor'), AlamatPerusahaan)

UI.ScreenShot("Informasi-Pekerjaan")

if (RT != '') {
	WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'RT (Opsional)']), RT)
}

if (RW != '') {
	WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'RW (Opsional)']), RW)
}

WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'Kode Pos Kantor']), PostCodeOffice)

WebUI.setText(findTestObject('Object Repository/Activation/Field_Box', [('field') : 'No Telp Kantor']), PhoneOffice)

if (Bank.toLowerCase() == 'bca') {
	if (NPWP != '') {
		WebUI.click(findTestObject('Object Repository/Activation/Radio_NPWP', [('text') : 'NPWP']))
		
		WebUI.setText(findTestObject('Object Repository/Activation/Field_NPWP'), NPWP)
		
	} else {
		WebUI.click(findTestObject('Object Repository/Activation/Radio_NPWP', [('text') : 'Tidak']))
	}
}

UI.ScreenShot("Informasi-Pekerjaan-2")

WebUI.click(findTestObject('Object Repository/Activation/Button_Lanjut'))

/** Buat Video **/
if (Bank.toLowerCase() != 'bca') {
	WebUI.click(findTestObject('Object Repository/Activation/Button_OpenCamera'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Activation/Button_StartRecord'))
	
	WebUI.delay(13)
	
	UI.ScreenShot("Record-video")
	
	WebUI.click(findTestObject('Object Repository/Activation/Button_Lanjut'))
}

/** Tanda Tangan **/
if (Bank.toLowerCase() == 'bca') {
	WebUI.delay(3)
	
	WebUI.click(findTestObject('Object Repository/Activation/Checkbox_Term', [('term') : 'Pernyataan']))
	
	WebUI.click(findTestObject('Object Repository/Activation/Checkbox_Term', [('term') : 'Rekening']))
	
	WebUI.click(findTestObject('Object Repository/Activation/Button_UploadBCA'))
	
	UI.UploadFile2(FileUploadBCA)

} else {
	WebUI.delay(3)

	UI.Signature(findTestObject('Object Repository/Activation/Canvas_Signature'))
	
	boolean ButtonSimpan = WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Button_Simpan'), 1, FailureHandling.OPTIONAL)
	
	if (ButtonSimpan) {
		WebUI.click(findTestObject('Object Repository/Activation/Button_Simpan'))
		
	} else {
		UI.ScreenShot("Missing Buttons")
		
		KeywordUtil.markFailedAndStop("Missing button Hapus and Simpan")
	}
}

WebUI.scrollToElement(findTestObject('Object Repository/Activation/Button_Lanjut'), 1)

UI.ScreenShot("get-Signature")

WebUI.click(findTestObject('Object Repository/Activation/Button_Lanjut'))

if (Complete) {
	WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Field_EnterPIN', [('div') : '1']), 30)
	
	for (int i = 1 ; i <= GlobalVariable.pin.size() ; i++) {
		int count = i - 1
		WebUI.setText(findTestObject('Object Repository/Activation/Field_EnterPIN', [('div') : i]), GlobalVariable.pin[count])
	}
	
	UI.ScreenShot("Enter-PIN-KYC")
	
	WebUI.click(findTestObject('Object Repository/Activation/Button_Submit'))
	
	WebUI.delay(3)
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/Register/Animation_Loading'), 30)
	
	WebUI.delay(1)
	
	boolean Alert = WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Alert_Error'), 2, FailureHandling.OPTIONAL)
	
	if (Alert) {
		WebUI.scrollToElement(findTestObject('Object Repository/Register/Alert_Error'), 1)
		
		UI.ScreenShot("Error Create KYC")
		
		String getError = WebUI.getText(findTestObject('Object Repository/Register/Alert_Error'))
		
		KeywordUtil.markFailedAndStop(getError)
	}
	
	boolean PopUp = WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Button_Beranda'), 30, FailureHandling.OPTIONAL)
	
	if (PopUp) {
		UI.ScreenShot("Popup-Beranda")
		
		WebUI.click(findTestObject('Object Repository/Activation/Button_Beranda'))
		
	} else {
		WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Button_Lanjut'), 10, FailureHandling.STOP_ON_FAILURE)
	
		WebUI.click(findTestObject('Object Repository/Activation/Button_Lanjut'))
		
		UI.ScreenShot("Popup-Beranda")
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/Activation/Button_Beranda'), 5)
		
		WebUI.click(findTestObject('Object Repository/Activation/Button_Beranda'))
	}
	
	WebUI.delay(3)
	
	UI.ScreenShot("Beranda")
	
	WebUI.closeBrowser()
}

