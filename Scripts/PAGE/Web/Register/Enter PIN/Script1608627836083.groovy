import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Field_PIN', [('pin') : '1']), 30)

for (int i = 1 ; i <= pinCode.size() ; i++) {
	int count = i - 1
	WebUI.setText(findTestObject('Object Repository/Register/Field_PIN', [('pin') : i]), pinCode[count])
}

UI.ScreenShot("Enter-PIN")

WebUI.click(findTestObject('Object Repository/Register/Button_Buat'))

for (int j = 1 ; j <= pinCode.size() ; j++) {
	int count = j - 1
	WebUI.setText(findTestObject('Object Repository/Register/Field_PIN', [('pin') : j]), pinCode[count])
}

UI.ScreenShot("Confirm-PIN")

WebUI.click(findTestObject('Object Repository/Register/Button_Konfirmasi'))

UI.GlobalVar('pin', pinCode)