import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI as UI

boolean result = false

int count = 0

if (Server.toLowerCase() == 'uat') {
    UI.RunBrowser(GlobalVariable.URL)
} else if (Server.toLowerCase() == 'dev') {
    UI.RunBrowser(GlobalVariable.URLdev)
} else if (Server.toLowerCase() == 'smoke') {
    UI.RunBrowser(GlobalVariable.URLSmoke)
}

WebUI.setText(findTestObject('Object Repository/Register/Field_PhoneNo'), PhoneNo)

WebUI.setText(findTestObject('Object Repository/Register/Field_Email'), Email)

WebUI.setText(findTestObject('Object Repository/Register/Field_Password'), Password)

WebUI.setText(findTestObject('Object Repository/Register/Field_ConfirmPassword'), ConfPass)

if (reffered != '') {
    WebUI.setText(findTestObject('Object Repository/Register/Field_RefferedCode'), reffered)
}

UI.ScreenShot('Sign-up')

WebUI.click(findTestObject('Object Repository/Register/Button_Daftar'))

if (checkValidation) {
    String checkNumber = UI.getValueDatabase('Postgre', Server, ('SELECT * FROM public.users_userinfo WHERE phone_number = \'' + 
        PhoneNo) + '\'', 'id')

    boolean NumberExist = WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Alert_Validation', [('content') : 'Nomor telepon telah terdaftar']), 
        1, FailureHandling.OPTIONAL)

    if ((checkNumber == 'null') && !(NumberExist)) {
        KeywordUtil.markPassed('Passed, Number is not used yet.')
    } else if ((checkNumber != 'null') && !(NumberExist)) {
        KeywordUtil.markWarning('Failed, The validation message of Phone Number should be displayed!')

        count += 1
    } else if ((checkNumber == 'null') && NumberExist) {
        KeywordUtil.markWarning('Failed, The validation message of Phone Number  shouldn\'t be displayed')

        count += 1
    } else if ((checkNumber != 'null') && NumberExist) {
        KeywordUtil.markPassed('Passed, Data Exist on Database and Validation Message Displayed')

        result = true
    }
    
    String checkEmail = UI.getValueDatabase('MySQL', Server, ('SELECT * FROM customer where customer_email = \'' + Email) + 
        '\'', 'customer_name')

    boolean EmailExist = WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Alert_Validation', [('content') : 'Email sudah terdaftar']), 
        1, FailureHandling.OPTIONAL)

    if ((checkEmail == 'null') && !(EmailExist)) {
        KeywordUtil.markPassed('Passed, Email is not used yet.')
    } else if ((checkEmail != 'null') && !(EmailExist)) {
        KeywordUtil.markWarning('Failed, The validation message of Email should be displayed!')

        count += 1
    } else if ((checkEmail == 'null') && EmailExist) {
        KeywordUtil.markWarning('Failed, The validation message of Email  shouldn\'t be displayed')

        count += 1
    } else if ((checkEmail != 'null') && EmailExist) {
        KeywordUtil.markPassed('Passed, Data Exist on Database and Validation Message Displayed')

        result = true
    }
    
    boolean ShortPass = WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Alert_Validation', [('content') : 'Kata sandi terlalu singkat']), 
        1, FailureHandling.OPTIONAL)

    boolean LowPass = WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Alert_Validation', [('content') : 'Kata sandi lemah']), 
        1, FailureHandling.OPTIONAL)

    boolean NumbPass = WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Alert_Validation', [('content') : 'Kata sandi ini seluruhnya terdiri dari angka']), 
        1, FailureHandling.OPTIONAL)

    boolean notMatch = WebUI.waitForElementVisible(findTestObject('Object Repository/Register/Alert_Validation', [('content') : 'Kedua bidang sandi tidak cocok']), 
        1, FailureHandling.OPTIONAL)

    /** password kurang dari 8 karakter **/
    if ((Password.size() < 8) && ShortPass) {
        KeywordUtil.markPassed('Passed, Validation Password is displayed.')

        result = true /** Password hanya angka **/
        /** Password hanya huruf **/
    } else if ((Password.size() < 8) && !(ShortPass)) {
        KeywordUtil.markWarning('Failed, The validation Password should be displayed!')

        count += 1
    } else if ((Password.size() >= 8) && ShortPass) {
        KeywordUtil.markWarning('Failed, The validation Password  shouldn\'t be displayed')

        count += 1
    } else if ((Password.size() >= 8) && !(ShortPass)) {
        if (((UI.checkNumber(Password) == true) && NumbPass) && LowPass) {
            KeywordUtil.markPassed('Passed, Validation Password is displayed.')

            result = true
        } else if (((UI.checkNumber(Password) == true) && !(NumbPass)) && !(LowPass)) {
            KeywordUtil.markWarning('Failed, The validation Password should be displayed!')

            count += 1
        } else if (((UI.checkNumber(Password) == false) && NumbPass) && LowPass) {
            KeywordUtil.markWarning('Failed, The validation Password  shouldn\'t be displayed')

            count += 1
        } else if (((UI.checkNumber(Password) == false) && !(NumbPass)) && !(LowPass)) {
            if ((UI.checkLetter(Password) == true) && LowPass) {
                KeywordUtil.markPassed('Passed, Validation Password is displayed.')

                result = true
            } else if ((UI.checkLetter(Password) == true) && !(LowPass)) {
                KeywordUtil.markWarning('Failed, The validation Password should be displayed!')

                count += 1
            } else if ((UI.checkLetter(Password) == false) && LowPass) {
                KeywordUtil.markWarning('Failed, The validation Password  shouldn\'t be displayed')

                count += 1
            } else if ((UI.checkLetter(Password) == false) && !(LowPass)) {
                KeywordUtil.markPassed('Passed, password can be used!')
            }
        }
    }
    
    /** Password match dengan konfirmasi Password **/
    if ((Password == ConfPass) && !(notMatch)) {
        KeywordUtil.markPassed('Password is Match with Password Confirmation')
    } else if ((Password == ConfPass) && notMatch) {
        KeywordUtil.markWarning('Failed, The validation Password not match shouldn\'t be displayed')

        count += 1
    } else if ((Password != ConfPass) && !(notMatch)) {
        KeywordUtil.markWarning('Failed, The validation Password not match should be displayed')

        count += 1
    } else {
        KeywordUtil.markPassed('Passed, Validation Password match is displayed.')

        result = true
    }
    
    if (count == 1) {
        KeywordUtil.markFailedAndStop('There is an error validation!')
    } else if (count > 1) {
        KeywordUtil.markFailedAndStop('There are some errors validation!')
    }
    
    if (result) {
        KeywordUtil.markPassed('Passed, All Validations are displayed properly')

        return null
    }
}

if (!(result)) {
    UI.GlobalVar('tempnomor', UI.getNumber() + 1)

    UI.updateGlobal('nomor', UI.getNumber())

    UI.GlobalVar('email', Email)
}

