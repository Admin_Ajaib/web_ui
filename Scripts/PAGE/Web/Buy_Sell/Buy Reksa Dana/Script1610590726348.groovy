import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

/** Buy Reksa Dana **/
WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Beranda']), 30)

UI.HoverItem(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Cari']))

UI.ScreenShot("Reksa Dana")

WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_SubHeader', [('menu') : 'Cari' , ('submenu') : 'Reksa Dana']))

UI.HoverItem(findTestObject('Buy_Sell/Reksa Dana/Field_ListReksaDana', [('product') : Stock]))

WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_Sell', [('product') : Stock]))

UI.GlobalVar("stock", Stock)

WebUI.delay(1)

/** Pop Up Buy **/
if (Price == '500.000' || Price == '1.000.000' || Price == '1.500.000') {
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_Price', [('price') : Price]))
	
} else {
	WebUI.setText(findTestObject('Buy_Sell/Reksa Dana/Input_Price'), Price)
}

UI.GlobalVar("price", Price + ',00')

if (PromoCode != '') {
	WebUI.setText(findTestObject('Buy_Sell/Reksa Dana/Input_KodePromo'), PromoCode)
}

UI.ScreenShot("Pop Up Setor")

WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_Next', [('action') : 'Setor']))

WebUI.delay(1)

if (payment.toLowerCase() == 'transfer') {
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Radio_Payment', [('payment') : 'Transfer Manual', ('pay') : payment]))
	
} else {
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Radio_Payment', [('payment') : 'Dompet Digital', ('pay') : payment]))
}

UI.ScreenShot("Popup Payment")

WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_Next', [('action') : 'Setor']))

WebUI.delay(2)

/** Check Transaction **/
if (payment.toLowerCase() == 'transfer') {
	WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Beranda']), 30)
	
	UI.HoverItem(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Transaksi']))
	
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_SubHeader', [('menu') : 'Transaksi' , ('submenu') : 'Reksa Dana']))
	
	boolean show = WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Field_CollapseShow'), 3, FailureHandling.OPTIONAL)
	
	while (!show) {
		WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_InProgress'))
		
		show = WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Field_CollapseShow'), 3, FailureHandling.OPTIONAL)
	}
	
	UI.ScreenShot("List Transaksi")
	
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_Upload', [('product') : Stock]))
	
	WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Button_OpenUpload'), 3)
	
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_OpenUpload'))
	
	UI.UploadFile2('ajaib.png')
	
	UI.ScreenShot("Upload File")
	
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_Next', [('action') : 'Unggah']))
	
	WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Button_LihatStatus'), 3)
	
	UI.ScreenShot("Popup Lihat Status")
	
	WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_LihatStatus'))
	
	WebUI.delay(3)
	
	show = WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Field_CollapseShow'), 3, FailureHandling.OPTIONAL)
	
	while (!show) {
		WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_InProgress'))
		
		show = WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Field_CollapseShow'), 3, FailureHandling.OPTIONAL)
	}
	
	String Status = WebUI.getText(findTestObject('Buy_Sell/Reksa Dana/Check_Status', [('product') : Stock]))
	
	UI.ScreenShot("Status Terbaru")

	if (Status == 'Bukti Setoran Terunggah') {
		UI.Note('Status is Correct')
		
	} else {
		KeywordUtil.markFailedAndStop('Status is Incorrect')
	}
}

WebUI.closeBrowser()
