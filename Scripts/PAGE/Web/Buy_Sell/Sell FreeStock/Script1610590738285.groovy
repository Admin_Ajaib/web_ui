import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI as UI
import com.keyword.WEB as WEB
import org.openqa.selenium.Keys as Keys

/** Sell Free Stock **/
WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Beranda']), 30)

WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Portofolio']))

WebUI.delay(1)

UI.ScreenShot('Portofolio')

boolean stockExist = WebUI.waitForElementVisible(findTestObject('Object Repository/Buy_Sell/FreeStock/Menu_Stock', [('product') : Stock]), 
    5, FailureHandling.OPTIONAL)

if (stockExist) {
	UI.HoverItem(findTestObject('Object Repository/Buy_Sell/FreeStock/Menu_Stock', [('product') : Stock]))
	
	UI.ScreenShot('Selected Stock')
	
    WebUI.click(findTestObject('Object Repository/Buy_Sell/FreeStock/Button_Sell', [('product') : Stock]))
} else {
    KeywordUtil.markFailedAndStop('Stock is not available')
}

/** Pop Up Sell **/
UI.ClickXpath('//span[text()=\'Tipe Order\']/parent::*/following-sibling::*/div[1]//*[text()=\'Limit Order\']')

UI.ClickXpath(('//span[text()=\'Tipe Order\']/parent::*/following-sibling::*/div[2]//*[text()=\'' + OrderType) + '\']')

UI.ClickXpath('//span[text()=\'Periode\']/parent::*/following-sibling::*/div[1]//*[text()=\'Hari\']')

UI.ClickXpath(('//span[text()=\'Periode\']/parent::*/following-sibling::*/div[2]//*[text()=\'' + Periode) + '\']')

String Harga = WebUI.getAttribute(UI.newTestObject('//span[text()=\'Harga (Rp)\']/parent::*/following-sibling::*/div[1]//input'), 'value')

if (harga != '') {
    UI.ClickXpath('//span[text()=\'Harga (Rp)\']/parent::*/following-sibling::*/div[1]//input')

    if (Harga != '0') {
		WebUI.sendKeys(UI.newTestObject('//span[text()=\'Harga (Rp)\']/parent::*/following-sibling::*/div[1]//input'), Keys.chord(Keys.CONTROL, 'a'))
		
        UI.WriteXpath('//span[text()=\'Harga (Rp)\']/parent::*/following-sibling::*/div[1]//input', harga)

		Harga = WebUI.getAttribute(UI.newTestObject('//span[text()=\'Harga (Rp)\']/parent::*/following-sibling::*/div[1]//input'), 'value')
    }
    
//    UI.WriteXpath('//span[text()=\'Harga (Rp)\']/parent::*/following-sibling::*/div[1]//input', harga)
	
//	Harga = WebUI.getAttribute(UI.newTestObject('//span[text()=\'Harga (Rp)\']/parent::*/following-sibling::*/div[1]//input'), 'value')
}

String TotalLot = WebUI.getText(findTestObject('Object Repository/Buy_Sell/FreeStock/Text_TotalLot'))
String getLot = '1'

if ((lot == '25%') || (lot == '25 %')) {
    UI.ClickXpath('//span[text()=\'25%\']')

    getLot = WebUI.getAttribute(UI.newTestObject('//span[text()=\'Lot\']/parent::*/following-sibling::*/div[1]//input'), 'value')

    WEB.checkLot(TotalLot, getLot, lot)
} else if ((lot == '50%') || (lot == '50 %')) {
    UI.ClickXpath('//span[text()=\'50%\']')

    getLot = WebUI.getAttribute(UI.newTestObject('//span[text()=\'Lot\']/parent::*/following-sibling::*/div[1]//input'), 'value')

    WEB.checkLot(TotalLot, getLot, lot)
} else if ((lot == '75%') || (lot == '75 %')) {
    UI.ClickXpath('//span[text()=\'75%\']')

    getLot = WebUI.getAttribute(UI.newTestObject('//span[text()=\'Lot\']/parent::*/following-sibling::*/div[1]//input'), 'value')

    WEB.checkLot(TotalLot, getLot, lot)
} else if ((lot == '100%') || (lot == '100 %')) {
    UI.ClickXpath('//span[text()=\'100%\']')

    getLot = WebUI.getAttribute(UI.newTestObject('//span[text()=\'Lot\']/parent::*/following-sibling::*/div[1]//input'), 'value')

    WEB.checkLot(TotalLot, getLot, lot)
} else if (lot != '1') {
    UI.ClearXpath('//span[text()=\'Lot\']/parent::*/following-sibling::*/div[1]//input')

    UI.WriteXpath('//span[text()=\'Lot\']/parent::*/following-sibling::*/div[1]//input', lot)
	
	getLot = lot
}

String TotalPrice = WebUI.getText(findTestObject('Object Repository/Buy_Sell/FreeStock/Text_TotalPrice'))

UI.ScreenShot('Pop up Jual')

WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_Next', [('action') : 'Jual']))

/** Pop Up Order Confirmation **/
String kodeSaham = WebUI.getText(findTestObject('Object Repository/Buy_Sell/FreeStock/Text_OrderConf', [('field') : 'Kode Saham']))
String Order = WebUI.getText(findTestObject('Object Repository/Buy_Sell/FreeStock/Text_OrderConf', [('field') : 'Aksi']))
String Period = WebUI.getText(findTestObject('Object Repository/Buy_Sell/FreeStock/Text_OrderConf', [('field') : 'Periode']))
String Price = WebUI.getText(findTestObject('Object Repository/Buy_Sell/FreeStock/Text_OrderConf', [('field') : Order + ' di Harga']))
String Lot = WebUI.getText(findTestObject('Object Repository/Buy_Sell/FreeStock/Text_OrderConf', [('field') : 'Lot']))
String Total = WebUI.getText(findTestObject('Object Repository/Buy_Sell/FreeStock/Text_OrderConf', [('field') : 'Total']))

ArrayList OrderConfirmation = [kodeSaham, Order, Period, Price, Lot, Total]
ArrayList SeedData = [Stock, 'Jual', Periode, 'Rp ' + Harga, getLot, TotalPrice]
ArrayList text = ['Kode Saham', 'Order', 'Periode', 'Harga', 'Lot', 'Total Harga']

for (int t = 0 ; t < OrderConfirmation.size() ; t++) {
	if (OrderConfirmation[t] == SeedData[t]) {
		WEB.Note("Passed, " + text[t] + " is match.")
		
	} else {
		KeywordUtil.markFailedAndStop("Failed, " + text[t] + " is not Match. It should be '" + SeedData[t] + "' instead of '" + OrderConfirmation[t] + "'")
	}
}

UI.ScreenShot('Konfirmasi Order')

WebUI.click(findTestObject('Object Repository/Buy_Sell/FreeStock/Button_Jual'))

/** Check Transaction **/
WebUI.waitForElementVisible(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Beranda']), 30)

UI.HoverItem(findTestObject('Buy_Sell/Reksa Dana/Button_Header', [('menu') : 'Transaksi']))

WebUI.click(findTestObject('Buy_Sell/Reksa Dana/Button_SubHeader', [('menu') : 'Transaksi' , ('submenu') : 'Saham Gratis']))

boolean show = WebUI.waitForElementVisible(findTestObject('Buy_Sell/FreeStock/Button_CollapseShow', [('text') : 'Order']), 3, FailureHandling.OPTIONAL)

while (!show) {
	WebUI.click(findTestObject('Object Repository/Buy_Sell/FreeStock/Button_Open', [('text') : 'Order (']))
	
	show = WebUI.waitForElementVisible(findTestObject('Buy_Sell/FreeStock/Button_CollapseShow', [('text') : 'Order']), 3, FailureHandling.OPTIONAL)
}

boolean checkOrder = WebUI.waitForElementVisible(findTestObject('Object Repository/Buy_Sell/FreeStock/List_Transaction', [('menu') : 'Order', ('num') : '4']), 3, FailureHandling.OPTIONAL)

UI.ScreenShot("List Transaksi")

if (checkOrder) {
	ArrayList display = WEB.getTableData(findTestObject('Object Repository/Buy_Sell/FreeStock/List_Transaction', [('menu') : 'Order', ('num') : '4']))

	display.remove(0)
	
	display.remove(display.size() - 1)
	
	ArrayList Data = ['Jual', kodeSaham, getLot, Price, Total]
	ArrayList header = ['Aksi', 'Kode Saham', 'Lot', 'Harga', 'Total']
	
	for (int x = 0 ; x < display.size(); x++) {
		if (display[x] == Data[x]) {
			WEB.Note("Passed, " + header[x] + " is match.")
			
		} else {
			KeywordUtil.markFailedAndStop("Failed, " + header[x] + " is not Match. It should be '" + Data[x] + "' instead of '" + display[x] + "'")
		}
	}
} else {
	KeywordUtil.markFailedAndStop("Order you created is not inserted on Order List")
}




