import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI as UI
import org.openqa.selenium.Keys as Keys

String url = WebUI.getUrl()

if (url != (GlobalVariable.adminurl + 'transactions/deposit/')) {
    UI.ScreenShot('Page not Accessed')

    KeywordUtil.markFailedAndStop('Page Deposits is not accessed!')
}

WebUI.waitForElementVisible(findTestObject('Object Repository/Admin-Tool/Check_Status', 
	[('email') : GlobalVariable.email,
    ('product') : GlobalVariable.stock, 
	('price') : GlobalVariable.price]), 30)

String CurrentStatus = WebUI.getText(findTestObject('Object Repository/Admin-Tool/Check_Status', 
	[('email') : GlobalVariable.email,
    ('product') : GlobalVariable.stock, 
	('price') : GlobalVariable.price]))

ArrayList AllStatus = ['WAITING_PAYMENT', 'PAYMENT_ACCEPTED', 'IN_PROGRESS', 'CONFIRMED']

while (CurrentStatus != Status) {
    AllStatus.remove(CurrentStatus)
	
	UI.ScreenShot("Status Order")
    
    WebUI.click(findTestObject('Object Repository/Admin-Tool/Button_OpenMenu', 
		[('email') : GlobalVariable.email, 
		('product') : GlobalVariable.stock,
        ('price') : GlobalVariable.price]))

	if (CurrentStatus == 'IN_PROGRESS') {
		WebUI.clearText(findTestObject('Object Repository/_dummy/dummy1'))

		UI.WriteXpath("//label[text()='Unit:']/following-sibling::*", "1.0000")
		
		UI.ScreenShot("Write Unit")
	}
	
    UI.ComboBoxOptionXpath('//label[text()=\'Status:\']/following-sibling::*', AllStatus[0])
	
	UI.ScreenShot("Change Status")

    WebUI.click(findTestObject('Object Repository/Admin-Tool/Button_Save'))

    WebUI.delay(2)

    CurrentStatus = WebUI.getText(findTestObject('Object Repository/Admin-Tool/Check_Status', 
		[('email') : GlobalVariable.email,
        ('product') : GlobalVariable.stock, 
		('price') : GlobalVariable.price]))
}

UI.ScreenShot("Status Order")

