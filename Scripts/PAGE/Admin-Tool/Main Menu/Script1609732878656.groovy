import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

boolean exist = WebUI.waitForElementClickable(findTestObject('Object Repository/Login/Admin Tool/Button_Main', 
	[('menu') : Menu,
	('action') : Action]), 3, FailureHandling.OPTIONAL)

if (!exist) {
	KeywordUtil.markFailedAndStop("Menu " + Menu + " is Not Found")
	
} else {
	WebUI.scrollToElement(findTestObject('Object Repository/Login/Admin Tool/Button_Main',
		[('menu') : Menu,
		('action') : Action]), 1)
	
	UI.ScreenShot("Main Menu")
	
	WebUI.click(findTestObject('Object Repository/Login/Admin Tool/Button_Main',
		[('menu') : Menu,
		('action') : Action]))
	
	WebUI.delay(2)
	
	String getURL = WebUI.getUrl()
	
	while (getURL == GlobalVariable.adminurl) {
		WebUI.click(findTestObject('Object Repository/Login/Admin Tool/Button_Main',
			[('menu') : Menu,
			('action') : Action]))
		
		WebUI.delay(2)
		getURL = WebUI.getUrl()
	}
}







