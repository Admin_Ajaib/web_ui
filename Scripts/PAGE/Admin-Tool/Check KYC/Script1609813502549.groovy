import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

String url = WebUI.getUrl()

if (url != GlobalVariable.adminurl + 'users/userinfo/') {
	UI.ScreenShot("Page not Accessed")
	KeywordUtil.markFailedAndStop("Page User Infos is not accessed!")
}

boolean exist = WebUI.waitForElementPresent(findTestObject('Object Repository/Admin-Tool/Text_KYC', 
	[('email') : email]), 2, FailureHandling.OPTIONAL)
	
if (!exist) {
	UI.ScreenShot("Failed - Status is Empty")
		
	KeywordUtil.markFailedAndStop("Failed, The status should be " + Expected + " instead of Empty.")
		
} else {
	String content = WebUI.getText(findTestObject('Object Repository/Admin-Tool/Text_KYC', 
		[('email') : email]))
		
	String[] result = content.split(" ")
	String status = result[1]
		
	if (status == Expected) {
		UI.ScreenShot("Status Passed")
			
		UI.Note('Passed, the status is correct')
	} else {
		UI.ScreenShot("Status Incorrect")
			
		KeywordUtil.markFailedAndStop("Failed, The status should be " + Expected + " instead of " + status)
	}
}
if (!checkOnly) {
	WebUI.click(findTestObject('Object Repository/Admin-Tool/Button_ID_UserInfos',
		[('email') : email]))
	
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos',
		[('menu') : "MF Status:"]), "VERIFIED")
	
	UI.ScreenShot("Status MF")
	
	WebUI.setText(findTestObject('Object Repository/Admin-Tool/Input_UserInfos',
		[('menu') : "Stock SID:"]), GlobalVariable.sid + UI.getSid())
	
	WebUI.setText(findTestObject('Object Repository/Admin-Tool/Input_UserInfos',
		[('menu') : "Depository (001):"]), GlobalVariable.sre + UI.getSre())
	
	WebUI.setText(findTestObject('Object Repository/Admin-Tool/Input_UserInfos',
		[('menu') : "Collateral (004):"]), GlobalVariable.sre + UI.getSre())
	
	UI.ScreenShot("Sid and Sre")
	
	UI.GlobalVar("tempinc", UI.getSid() + 1)
	
	UI.updateGlobal("inc", UI.getSid())
	
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos2',
		[('menu') : "Status CBEST"]), CBEST)
	
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos2',
		[('menu') : "Status RDN"]), RDN)
	
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos2',
		[('menu') : "Status FGS"]), FGS)
	
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos2',
		[('menu') : "Status S21"]), S21)
	
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos2',
		[('menu') : "Account Status"]), Account)
	
	UI.ScreenShot("Stock KYC Status")
	/*
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos2',
		[('menu') : "Premium Status"]), Premium)*/
	
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos2',
		[('menu') : "Buy Enable Status"]), Buy)
	
	UI.ComboBoxOption(findTestObject('Object Repository/Admin-Tool/Dropdown_UserInfos2',
		[('menu') : "Sell Enable Status"]), Sell)
	
	UI.ScreenShot("Stock Trade Status")
	
	WebUI.click(findTestObject('Object Repository/Admin-Tool/Button_Save'))
	
	UI.ScreenShot("Data Saved")
}