import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

if (Server.toLowerCase() == 'uat') {
	UI.RunBrowser(GlobalVariable.Adminuat + "login/?next=/admin/")
	UI.GlobalVar('adminurl', GlobalVariable.Adminuat)
	
} else if (Server.toLowerCase() == 'dev') {
	UI.RunBrowser(GlobalVariable.Admindev + "/login/?next=/admin/")
	UI.GlobalVar('adminurl', GlobalVariable.Admindev)
}

WebUI.setText(findTestObject('Object Repository/Login/Admin Tool/Field_user_pass', [('field') : 'username']), 'secret')

WebUI.setText(findTestObject('Object Repository/Login/Admin Tool/Field_user_pass', [('field') : 'password']), 'Ajaib123')

UI.ScreenShot("Sign-In Admin-tool")

WebUI.click(findTestObject('Object Repository/Register/Button_Daftar'))







