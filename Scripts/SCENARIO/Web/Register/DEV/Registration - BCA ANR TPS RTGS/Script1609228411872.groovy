import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

WebUI.callTestCase(findTestCase('PAGE/Web/Register/SignUp'), 
	[('Server') : 'DEV',
	('PhoneNo') : GlobalVariable.Phone + UI.getNumber(), 
	('Email') : GlobalVariable.username + UI.getNumber() +'@ajaib.co.id', 
	('Password') : GlobalVariable.username + '1234', 
	('ConfPass') : GlobalVariable.username + '1234',
	('checkValidation') : false,
	('reffered') : ''], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/Web/Register/Enter PIN'), 
	[('pinCode') : '1234'], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/Web/Register/Activation'), 
	[('NIK') : GlobalVariable.civil + UI.getNik().toString(), 
	('FileUpload') : 'Snake_River_(5mb).jpg', 
	('education') : 'S1', 
	('Nama') : 'ANR TPS RTGS',
	('TempatLahir') : 'Yogyakarta',
	('Alamat') : 'Jalan Pelan-pelan',
	('Kota') : 'Yogyakarta',
	('KodePos') : '55141',
	('IbuKandung') : 'Taylor Swift',
	('TotalIncome') : '20 juta',
	('Toleransi') : 'sedang',
	('Pengalaman') : 'Tidak',
	('JangkaWaktu') : '1 sampai 3 tahun',
	('Bank') : 'BCA',
	('Rekening') : '0613336939',
	('Pekerjaan') : 'Wiraswasta',
	('Perusahaan') : 'PT. Ajaib',
	('Bidang') : 'Restoran',
	('AlamatPerusahaan') : 'Jakarta Barat',
	('RT') : '',
	('RW') : '',
	('PostCodeOffice') : '55141',
	('PhoneOffice') : '021385176',
	('NPWP') : '378219462819321',
	('FileUploadBCA') : 'mario_PNG53.png',
	('Complete') : true], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/Admin-Tool/Login'),
	[('Server') : 'DEV'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/Admin-Tool/Main Menu'),
	[('Menu') : 'User Infos',
	('Action') : 'Change'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/Admin-Tool/Check KYC'),
	[('checkOnly') : false,
	('email') : GlobalVariable.email,
	('Expected') : 'KYC',
	('CBEST') : 'Active',
	('RDN') : 'Active',
	('FGS') : 'Active',
	('S21') : 'Active',
	('Account') : 'Active',
	('Premium') : 'Premium 30',
	('Buy') : 'Active',
	('Sell') : 'Active',],
	FailureHandling.STOP_ON_FAILURE)