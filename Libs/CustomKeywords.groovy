
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject

import com.kms.katalon.core.model.FailureHandling

import com.applitools.eyes.RectangleSize

import com.applitools.eyes.selenium.Eyes



def static "com.keyword.WEB.checkLot"(
    	String TotalLot	
     , 	String currentLot	
     , 	String percent	) {
    (new com.keyword.WEB()).checkLot(
        	TotalLot
         , 	currentLot
         , 	percent)
}


def static "com.keyword.WEB.getWatchlist"(
    	TestObject tableXpath	) {
    (new com.keyword.WEB()).getWatchlist(
        	tableXpath)
}


def static "com.keyword.WEB.getStockList"(
    	TestObject tableXpath	) {
    (new com.keyword.WEB()).getStockList(
        	tableXpath)
}


def static "com.keyword.WEB.getTableData"(
    	TestObject tableXpath	) {
    (new com.keyword.WEB()).getTableData(
        	tableXpath)
}


def static "com.katalon.plugin.keyword.calendar.SetDateCalendarKeyword.setDate"(
    	TestObject to	
     , 	int day	
     , 	int month	
     , 	int year	
     , 	int slideTimeOut	
     , 	FailureHandling flowControl	) {
    (new com.katalon.plugin.keyword.calendar.SetDateCalendarKeyword()).setDate(
        	to
         , 	day
         , 	month
         , 	year
         , 	slideTimeOut
         , 	flowControl)
}


def static "com.keyword.UI.connectDB"(
    	String database	
     , 	String environment	) {
    (new com.keyword.UI()).connectDB(
        	database
         , 	environment)
}


def static "com.keyword.UI.closeDatabaseConnection"() {
    (new com.keyword.UI()).closeDatabaseConnection()
}


def static "com.keyword.UI.executeQuery"(
    	String queryString	) {
    (new com.keyword.UI()).executeQuery(
        	queryString)
}


def static "com.keyword.UI.execute"(
    	String queryString	) {
    (new com.keyword.UI()).execute(
        	queryString)
}


def static "com.keyword.UI.countdbRow"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbRow(
        	queryTable)
}


def static "com.keyword.UI.getValueDatabase"(
    	String database	
     , 	String environment	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getValueDatabase(
        	database
         , 	environment
         , 	queryTable
         , 	ColumnName)
}


def static "com.keyword.UI.getOneColumnDatabase"(
    	String database	
     , 	String environment	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getOneColumnDatabase(
        	database
         , 	environment
         , 	queryTable
         , 	ColumnName)
}


def static "com.keyword.UI.RunBrowser"(
    	String URL	) {
    (new com.keyword.UI()).RunBrowser(
        	URL)
}


def static "com.keyword.UI.newTestObject"(
    	String locator	) {
    (new com.keyword.UI()).newTestObject(
        	locator)
}


def static "com.keyword.UI.HoverItem"(
    	TestObject xpath	) {
    (new com.keyword.UI()).HoverItem(
        	xpath)
}


def static "com.keyword.UI.ClickXpath"(
    	String xpath	) {
    (new com.keyword.UI()).ClickXpath(
        	xpath)
}


def static "com.keyword.UI.ClearXpath"(
    	String xpath	) {
    (new com.keyword.UI()).ClearXpath(
        	xpath)
}


def static "com.keyword.UI.Click"(
    	TestObject xpath	) {
    (new com.keyword.UI()).Click(
        	xpath)
}


def static "com.keyword.UI.WriteXpath"(
    	String xpath	
     , 	String text	) {
    (new com.keyword.UI()).WriteXpath(
        	xpath
         , 	text)
}


def static "com.keyword.UI.ComboBoxOption"(
    	TestObject Combo	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxOption(
        	Combo
         , 	Value)
}


def static "com.keyword.UI.ComboBoxOptionXpath"(
    	String Combo	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxOptionXpath(
        	Combo
         , 	Value)
}


def static "com.keyword.UI.datePicker"(
    	String date	) {
    (new com.keyword.UI()).datePicker(
        	date)
}


def static "com.keyword.UI.Sleep"(
    	int timeOut	) {
    (new com.keyword.UI()).Sleep(
        	timeOut)
}


def static "com.keyword.UI.UploadFile2"(
    	String pictureName	) {
    (new com.keyword.UI()).UploadFile2(
        	pictureName)
}


def static "com.keyword.UI.getDateToday"(
    	String format	) {
    (new com.keyword.UI()).getDateToday(
        	format)
}


def static "com.keyword.UI.ScreenShot"(
    	String FileName	) {
    (new com.keyword.UI()).ScreenShot(
        	FileName)
}


def static "com.keyword.UI.GlobalVar"(
    	String name	
     , 	Object value	) {
    (new com.keyword.UI()).GlobalVar(
        	name
         , 	value)
}


def static "com.keyword.UI.Note"(
    	Object variable	) {
    (new com.keyword.UI()).Note(
        	variable)
}


def static "com.keyword.UI.updatePermanently"(
    	String envName	
     , 	String varName	
     , 	Object newValue	) {
    (new com.keyword.UI()).updatePermanently(
        	envName
         , 	varName
         , 	newValue)
}


def static "com.keyword.UI.updateGlobal"(
    	String varName	
     , 	Object newValue	) {
    (new com.keyword.UI()).updateGlobal(
        	varName
         , 	newValue)
}


def static "com.keyword.UI.Signature"(
    	TestObject Canvas	) {
    (new com.keyword.UI()).Signature(
        	Canvas)
}


def static "com.keyword.UI.forceStop"(
    	String comment	) {
    (new com.keyword.UI()).forceStop(
        	comment)
}


def static "com.keyword.UI.getNumber"() {
    (new com.keyword.UI()).getNumber()
}


def static "com.keyword.UI.getNik"() {
    (new com.keyword.UI()).getNik()
}


def static "com.keyword.UI.getSid"() {
    (new com.keyword.UI()).getSid()
}


def static "com.keyword.UI.getSre"() {
    (new com.keyword.UI()).getSre()
}


def static "com.keyword.UI.killProcess"() {
    (new com.keyword.UI()).killProcess()
}


def static "com.keyword.UI.checkNumber"(
    	String text	) {
    (new com.keyword.UI()).checkNumber(
        	text)
}


def static "com.keyword.UI.checkLetter"(
    	String text	) {
    (new com.keyword.UI()).checkLetter(
        	text)
}


def static "com.keyword.UI.dragNdrop"(
    	TestObject source	
     , 	TestObject target	) {
    (new com.keyword.UI()).dragNdrop(
        	source
         , 	target)
}


def static "com.kms.katalon.keyword.applitools.BasicKeywords.checkWindow"(
    	String testName	) {
    (new com.kms.katalon.keyword.applitools.BasicKeywords()).checkWindow(
        	testName)
}


def static "com.kms.katalon.keyword.applitools.BasicKeywords.checkTestObject"(
    	TestObject testObject	
     , 	String testName	) {
    (new com.kms.katalon.keyword.applitools.BasicKeywords()).checkTestObject(
        	testObject
         , 	testName)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesOpenWithBaseline"(
    	String baselineName	
     , 	String testName	
     , 	RectangleSize viewportSize	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesOpenWithBaseline(
        	baselineName
         , 	testName
         , 	viewportSize)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesInit"() {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesInit()
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesClose"(
    	Eyes eyes	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesClose(
        	eyes)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesOpen"(
    	String testName	
     , 	RectangleSize viewportSize	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesOpen(
        	testName
         , 	viewportSize)
}
