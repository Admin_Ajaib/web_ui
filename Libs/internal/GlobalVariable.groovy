package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object URL
     
    /**
     * <p></p>
     */
    public static Object URLdev
     
    /**
     * <p></p>
     */
    public static Object URLSmoke
     
    /**
     * <p></p>
     */
    public static Object Admindev
     
    /**
     * <p></p>
     */
    public static Object Adminuat
     
    /**
     * <p></p>
     */
    public static Object nomor
     
    /**
     * <p></p>
     */
    public static Object nik
     
    /**
     * <p></p>
     */
    public static Object Phone
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object civil
     
    /**
     * <p></p>
     */
    public static Object sid
     
    /**
     * <p></p>
     */
    public static Object sre
     
    /**
     * <p></p>
     */
    public static Object inc
     
    /**
     * <p></p>
     */
    public static Object pathScreenshot
     
    /**
     * <p></p>
     */
    public static Object mysqldevip
     
    /**
     * <p></p>
     */
    public static Object postgredevip
     
    /**
     * <p></p>
     */
    public static Object mysqluatip
     
    /**
     * <p></p>
     */
    public static Object postgreuatip
     
    /**
     * <p></p>
     */
    public static Object mysqldevusername
     
    /**
     * <p></p>
     */
    public static Object postgredevusername
     
    /**
     * <p></p>
     */
    public static Object mysqluatusername
     
    /**
     * <p></p>
     */
    public static Object postgreuatusername
     
    /**
     * <p></p>
     */
    public static Object mysqldevpass
     
    /**
     * <p></p>
     */
    public static Object postgredevpass
     
    /**
     * <p></p>
     */
    public static Object mysqluatpass
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            URL = selectedVariables['URL']
            URLdev = selectedVariables['URLdev']
            URLSmoke = selectedVariables['URLSmoke']
            Admindev = selectedVariables['Admindev']
            Adminuat = selectedVariables['Adminuat']
            nomor = selectedVariables['nomor']
            nik = selectedVariables['nik']
            Phone = selectedVariables['Phone']
            username = selectedVariables['username']
            civil = selectedVariables['civil']
            sid = selectedVariables['sid']
            sre = selectedVariables['sre']
            inc = selectedVariables['inc']
            pathScreenshot = selectedVariables['pathScreenshot']
            mysqldevip = selectedVariables['mysqldevip']
            postgredevip = selectedVariables['postgredevip']
            mysqluatip = selectedVariables['mysqluatip']
            postgreuatip = selectedVariables['postgreuatip']
            mysqldevusername = selectedVariables['mysqldevusername']
            postgredevusername = selectedVariables['postgredevusername']
            mysqluatusername = selectedVariables['mysqluatusername']
            postgreuatusername = selectedVariables['postgreuatusername']
            mysqldevpass = selectedVariables['mysqldevpass']
            postgredevpass = selectedVariables['postgredevpass']
            mysqluatpass = selectedVariables['mysqluatpass']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
