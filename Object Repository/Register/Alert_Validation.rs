<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Alert_Validation</name>
   <tag></tag>
   <elementGuidId>58087877-0454-4cb3-8dbd-2de8fa68752a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;alert alert-danger&quot; and contains(text(),&quot;${content}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;alert alert-danger&quot; and contains(text(),&quot;${content}&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
