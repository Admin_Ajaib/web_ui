<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Collapse</name>
   <tag></tag>
   <elementGuidId>143e130c-e72a-4ade-9880-a9a6efd979b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()=&quot;Watchlist :&quot;]/ancestor::div[2]/following-sibling::div/div[${button}]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()=&quot;Watchlist :&quot;]/ancestor::div[2]/following-sibling::div/div[${button}]</value>
   </webElementProperties>
</WebElementEntity>
