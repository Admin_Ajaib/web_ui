<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text_Subtitle</name>
   <tag></tag>
   <elementGuidId>f39bf732-2a81-4925-8046-4d373baa1e2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class,&quot;subtitle&quot;) and text()=&quot;${name}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(@class,&quot;subtitle&quot;) and text()=&quot;${name}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
