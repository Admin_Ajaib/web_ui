<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Popup2</name>
   <tag></tag>
   <elementGuidId>6a233b65-16b5-424e-9a91-d8decd57acbc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]//button[text()=&quot;${button}&quot; and not(@disabled)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]//button[text()=&quot;${button}&quot; and not(@disabled)]</value>
   </webElementProperties>
</WebElementEntity>
