<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_SortWatchlist</name>
   <tag></tag>
   <elementGuidId>e9a27dc9-9ed8-42a1-a729-0ffdf7ed9e06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Kode Saham&quot;]/following-sibling::img${button}</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Kode Saham&quot;]/following-sibling::img${button}</value>
   </webElementProperties>
</WebElementEntity>
