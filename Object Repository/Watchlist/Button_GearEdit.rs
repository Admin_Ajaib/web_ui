<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_GearEdit</name>
   <tag></tag>
   <elementGuidId>b2fcd2b0-fcc1-4d4b-8ebd-1e6e69433786</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//*[text()=&quot;Watchlist :&quot;]/ancestor::div[2]/following-sibling::div/div[1]//img)[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[text()=&quot;Watchlist :&quot;]/ancestor::div[2]/following-sibling::div/div[1]//img)[1]</value>
   </webElementProperties>
</WebElementEntity>
