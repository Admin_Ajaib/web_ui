<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox_Remove Stocks</name>
   <tag></tag>
   <elementGuidId>ea920b0f-b14a-4f69-b6d6-ccc31d1b26bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;watchlist-container&quot;]//span[text()=&quot;${stock}&quot;]/parent::div/preceding-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;watchlist-container&quot;]//span[text()=&quot;${stock}&quot;]/parent::div/preceding-sibling::input</value>
   </webElementProperties>
</WebElementEntity>
