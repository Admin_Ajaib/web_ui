<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Container_Watchlist</name>
   <tag></tag>
   <elementGuidId>3fa6f32d-6194-4fc1-88f6-94af783ec01f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;watchlist-group-container&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;watchlist-group-container&quot;]</value>
   </webElementProperties>
</WebElementEntity>
