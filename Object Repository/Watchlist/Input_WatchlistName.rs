<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_WatchlistName</name>
   <tag></tag>
   <elementGuidId>f5f19831-e11d-4820-936a-bce603adb579</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[contains(@placeholder,&quot;Watchlist baru&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[contains(@placeholder,&quot;Watchlist baru&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
