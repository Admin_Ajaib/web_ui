<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Stock_nonBonus</name>
   <tag></tag>
   <elementGuidId>8036553b-7da5-40d2-a520-301c25916618</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@id,'stock')]//span[text()=&quot;${product}&quot; and not(following-sibling::div[text()=&quot;Bonus&quot;])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@id,'stock')]//span[text()=&quot;${product}&quot; and not(following-sibling::div[text()=&quot;Bonus&quot;])]</value>
   </webElementProperties>
</WebElementEntity>
