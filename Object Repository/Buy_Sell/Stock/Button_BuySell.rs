<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_BuySell</name>
   <tag></tag>
   <elementGuidId>888d4986-c8b2-45ba-b967-f46d441ecf8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()=&quot;${action}&quot; and not (contains(@class,&quot;rounded&quot;))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()=&quot;${action}&quot; and not (contains(@class,&quot;rounded&quot;))]</value>
   </webElementProperties>
</WebElementEntity>
