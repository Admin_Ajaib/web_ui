<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Stock_BuySell</name>
   <tag></tag>
   <elementGuidId>94cf6097-7001-471b-a868-515b730569bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@id,'stock')]//span[text()=&quot;${product}&quot; and not(following-sibling::div[text()=&quot;Bonus&quot;])]/ancestor::div[2]/following-sibling::div//button[text()=&quot;${action}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@id,'stock')]//span[text()=&quot;${product}&quot; and not(following-sibling::div[text()=&quot;Bonus&quot;])]/ancestor::div[2]/following-sibling::div//button[text()=&quot;${action}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
