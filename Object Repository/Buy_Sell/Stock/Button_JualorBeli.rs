<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_JualorBeli</name>
   <tag></tag>
   <elementGuidId>75373567-3584-4b39-99a8-1c217c11e293</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;root&quot;]/div[4]/div/div[3]/div[4]/div//button[text()=&quot;${action}&quot; and not(contains(@class,'rounded'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;root&quot;]/div[4]/div/div[3]/div[4]/div//button[text()=&quot;${action}&quot; and not(contains(@class,'rounded'))]</value>
   </webElementProperties>
</WebElementEntity>
