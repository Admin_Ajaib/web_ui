<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button _Product</name>
   <tag></tag>
   <elementGuidId>74214ce4-e2f3-4caa-882c-a0cb9c2c2f6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'main-container')]//span[text()=&quot;${product}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'main-container')]//span[text()=&quot;${product}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
