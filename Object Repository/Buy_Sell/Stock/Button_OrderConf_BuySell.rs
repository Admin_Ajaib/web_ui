<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_OrderConf_BuySell</name>
   <tag></tag>
   <elementGuidId>6cee66a6-abcf-49b6-8888-b2e244f059e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Konfirmasi Order&quot;]//ancestor::div[3]//button[text()=&quot;${action}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Konfirmasi Order&quot;]//ancestor::div[3]//button[text()=&quot;${action}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
