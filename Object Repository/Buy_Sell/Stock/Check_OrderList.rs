<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Check_OrderList</name>
   <tag></tag>
   <elementGuidId>793b9b32-b788-4b2c-ad80-1a2014ddb81b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),'${menu}')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]/div/div/div[1]/div[2]/div[1]/div/div[1]/div/div[3]/span[text()='${product}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),'${menu}')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]/div/div/div[1]/div[2]/div[1]/div/div[1]/div/div[3]/span[text()='${product}']</value>
   </webElementProperties>
</WebElementEntity>
