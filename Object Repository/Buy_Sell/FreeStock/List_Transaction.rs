<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>List_Transaction</name>
   <tag></tag>
   <elementGuidId>6d8ca241-f604-444f-9475-69c874ab90c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),'${menu}')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]//span[text()='Waktu']/ancestor::div[2]/following-sibling::div/descendant::div[${num}]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),'${menu}')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]//span[text()='Waktu']/ancestor::div[2]/following-sibling::div/descendant::div[${num}]</value>
   </webElementProperties>
</WebElementEntity>
