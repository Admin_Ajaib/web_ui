<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text_Details</name>
   <tag></tag>
   <elementGuidId>660cdf99-b972-42b1-8e67-510beb39a823</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),'${menu}')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]/div/div/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),'${menu}')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]/div/div/div[1]</value>
   </webElementProperties>
</WebElementEntity>
