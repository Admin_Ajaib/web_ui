<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text_TotalLot</name>
   <tag></tag>
   <elementGuidId>a1bdadb8-5cff-4cf5-8f61-7ff0b77c7331</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()='Lot Dimiliki']/parent::*/following-sibling::div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='Lot Dimiliki']/parent::*/following-sibling::div/span</value>
   </webElementProperties>
</WebElementEntity>
