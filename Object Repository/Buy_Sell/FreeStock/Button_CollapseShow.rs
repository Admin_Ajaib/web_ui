<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_CollapseShow</name>
   <tag></tag>
   <elementGuidId>b4558b4b-6eaf-4751-8ab0-3c9ddda0f8d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),'${text}')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),'${text}')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]</value>
   </webElementProperties>
</WebElementEntity>
