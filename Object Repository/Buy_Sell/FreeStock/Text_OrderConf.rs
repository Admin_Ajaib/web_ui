<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text_OrderConf</name>
   <tag></tag>
   <elementGuidId>ece1e4ac-515f-48cc-bccf-12f7ba22478e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Konfirmasi Order&quot;]//ancestor::div[3]//span[text()=&quot;${field}&quot;]/parent::*/following-sibling::*/*</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Konfirmasi Order&quot;]//ancestor::div[3]//span[text()=&quot;${field}&quot;]/parent::*/following-sibling::*/*</value>
   </webElementProperties>
</WebElementEntity>
