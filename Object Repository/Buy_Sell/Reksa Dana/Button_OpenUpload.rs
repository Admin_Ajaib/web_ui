<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_OpenUpload</name>
   <tag></tag>
   <elementGuidId>79002f94-6e0c-4ddc-85ab-3305f9d834c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),'bukti transfer')]/preceding-sibling::div//img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),'bukti transfer')]/preceding-sibling::div//img</value>
   </webElementProperties>
</WebElementEntity>
