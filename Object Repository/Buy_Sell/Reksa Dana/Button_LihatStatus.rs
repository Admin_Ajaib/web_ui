<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_LihatStatus</name>
   <tag></tag>
   <elementGuidId>793963b3-645a-4eda-ad0d-6d910c5f7bba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[contains(text(),'Bukti Transfer')]//ancestor::div[2]//button[text()=&quot;Lihat Status&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[contains(text(),'Bukti Transfer')]//ancestor::div[2]//button[text()=&quot;Lihat Status&quot;])[2]</value>
   </webElementProperties>
</WebElementEntity>
