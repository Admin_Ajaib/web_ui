<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_KodePromo</name>
   <tag></tag>
   <elementGuidId>d1aefb83-78a7-4f49-b6de-8f516d1ea6a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Setor&quot;]//ancestor::div[5]//input[contains(@placeholder,'kode promo')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Setor&quot;]//ancestor::div[5]//input[contains(@placeholder,'kode promo')]</value>
   </webElementProperties>
</WebElementEntity>
