<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Upload</name>
   <tag></tag>
   <elementGuidId>a65af516-be5e-4a9d-9e14-ee278621d447</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),'Dalam Proses')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]//span[text()='${product}']//ancestor::div[3]//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),'Dalam Proses')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]//span[text()='${product}']//ancestor::div[3]//button</value>
   </webElementProperties>
</WebElementEntity>
