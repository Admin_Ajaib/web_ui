<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Check_Status</name>
   <tag></tag>
   <elementGuidId>90d47610-ca4a-482c-b53a-645c8e98e860</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[contains(text(),'Dalam Proses')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]//span[text()='${product}']//ancestor::div[3]/div[5])[1]//span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[contains(text(),'Dalam Proses')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]//span[text()='${product}']//ancestor::div[3]/div[5])[1]//span</value>
   </webElementProperties>
</WebElementEntity>
