<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Radio_Payment</name>
   <tag></tag>
   <elementGuidId>6692d98c-03d1-429d-b301-c8bfedc251b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Metode Pembayaran&quot;]//ancestor::div[3]//span[text()='${payment}']//ancestor::div[3]//span[text()='${pay}']/following-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Metode Pembayaran&quot;]//ancestor::div[3]//span[text()='${payment}']//ancestor::div[3]//span[text()='${pay}']/following-sibling::input</value>
   </webElementProperties>
</WebElementEntity>
