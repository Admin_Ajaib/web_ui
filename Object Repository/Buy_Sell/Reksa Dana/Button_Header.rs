<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Header</name>
   <tag></tag>
   <elementGuidId>4d528762-bfb6-4ee9-aec9-865c3ee7069f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li//a[text()=&quot;${menu}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li//a[text()=&quot;${menu}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
