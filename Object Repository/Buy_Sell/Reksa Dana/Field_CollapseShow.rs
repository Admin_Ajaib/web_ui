<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Field_CollapseShow</name>
   <tag></tag>
   <elementGuidId>6369d7d3-0fb3-4b65-9f31-068920c49221</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),'Dalam Proses')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),'Dalam Proses')]//ancestor::div[4]/div[@class=&quot;collapse show&quot;]</value>
   </webElementProperties>
</WebElementEntity>
