<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_ActivateNowPortofolio</name>
   <tag></tag>
   <elementGuidId>da634c5a-44b8-47e3-bb00-fd62531e2699</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(),&quot;Dalam Proses&quot;)]/ancestor::div[4]//button[text()=&quot;Aktivasi Sekarang&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(),&quot;Dalam Proses&quot;)]/ancestor::div[4]//button[text()=&quot;Aktivasi Sekarang&quot;]</value>
   </webElementProperties>
</WebElementEntity>
