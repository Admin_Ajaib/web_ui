<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Field_EnterPIN</name>
   <tag></tag>
   <elementGuidId>6c237161-429d-40ce-ba2f-43cb09297cb4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()=&quot;Masukkan PIN&quot;]/ancestor::div[2]/div//input[${div}]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()=&quot;Masukkan PIN&quot;]/ancestor::div[2]/div//input[${div}]</value>
   </webElementProperties>
</WebElementEntity>
