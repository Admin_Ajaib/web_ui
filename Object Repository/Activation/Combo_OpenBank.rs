<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo_OpenBank</name>
   <tag></tag>
   <elementGuidId>e9f168e3-61bc-4482-857b-c8afe25af379</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()=&quot;Nama Bank&quot;]/parent::div/div[contains(@class,'container')]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()=&quot;Nama Bank&quot;]/parent::div/div[contains(@class,'container')]/div</value>
   </webElementProperties>
</WebElementEntity>
