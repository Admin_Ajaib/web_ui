<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_enableBCA</name>
   <tag></tag>
   <elementGuidId>0ff01950-9a54-4093-bfdc-1f7cf2753114</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()='${action}' and not(@class)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()='${action}' and not(@class)]</value>
   </webElementProperties>
</WebElementEntity>
