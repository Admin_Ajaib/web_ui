<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Main</name>
   <tag></tag>
   <elementGuidId>21bb6c9d-7374-4b61-95c7-0f7dc8e5546a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()='${menu}' and not(@class)]/ancestor::tr//a[text()='${action}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()='${menu}' and not(@class)]/ancestor::tr//a[text()='${action}']</value>
   </webElementProperties>
</WebElementEntity>
