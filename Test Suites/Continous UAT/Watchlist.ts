<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Watchlist</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>052dced8-b284-4a72-ad04-19eedc481e88</testSuiteGuid>
   <testCaseLink>
      <guid>7115e999-f218-4f47-b213-45441a9d9270</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Watchlist/UAT/Create Watchlist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2df2874-8773-4e43-9da1-b16e77a9f222</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Watchlist/DEV/Rearrange Watchlist - Sort Asc - Desc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20cf7022-fadf-49b0-84f4-abf47b49e022</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Watchlist/UAT/Rearrange Watchlist - Up and Down</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>753508ea-fc97-4db6-a5a5-6338f88868ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Watchlist/DEV/Rearrange Watchlist - Delete Stock</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ebde36a-999f-4b08-86e8-e3a9b94a612e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Watchlist/UAT/Edit Watchlist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d27d3f26-255a-4be7-84c4-2db59ccaab9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Watchlist/UAT/Delete Watchlist</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
