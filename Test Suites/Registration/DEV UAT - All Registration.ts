<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DEV UAT - All Registration</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>fba52fd4-c271-499d-9aa7-a2538c103254</testSuiteGuid>
   <testCaseLink>
      <guid>fee6a992-7292-4972-8977-69c07cd7a792</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1722bb61-06d5-4994-803d-54dc8c856b64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>daef5b70-ace6-41c7-b88f-1c809979838e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e3d8557-026d-4c58-8484-52aca01d42f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0fc288c-1f57-4c78-91aa-4ff24c6963f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6c35073-fac2-4287-ae1b-bed6e84e0a07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>42e9c837-5077-4087-a8b0-fa01295e1f99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - Permata</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8309cc6b-2ed8-4656-929a-1c7f0910f706</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/UAT/Registration - Permata</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
