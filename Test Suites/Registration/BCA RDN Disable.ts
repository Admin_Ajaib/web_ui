<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BCA RDN Disable</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f5c33e7a-246d-4af0-a735-e3f5ac2a2374</testSuiteGuid>
   <testCaseLink>
      <guid>a9f79765-a133-4fc1-aa32-72eab9ec5552</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Admin-tool/Switch BCA RDN/DEV/Deactivate BCA RDN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8184e8b-a948-417d-b86a-92a630614829</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2dd47bd-27ce-436d-94b1-23e0d09286a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - Permata</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
