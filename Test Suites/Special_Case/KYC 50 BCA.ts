<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>KYC 50 BCA</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9b252419-1bb5-4f0a-93e7-c2b3985b157a</testSuiteGuid>
   <testCaseLink>
      <guid>a0d0f557-98d2-452f-ae34-14069081a0db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d1a0ee23-a879-4763-ad83-4213e9a46079</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efc34d54-2035-4cee-b95f-b00c57138436</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ce3ef58-3aee-4140-9412-93811e68b179</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db01d1ed-2cbb-4996-b558-493d9a632399</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86fdf13f-01bc-4e4f-9933-74ced1f68c54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d00875c0-d3f6-42ca-80d2-01d5175cba41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>57f4b7db-e452-4034-bc42-1b38c6b52038</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e27801fb-8424-4501-94c6-60c182eabba3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e866caf6-1de5-4225-ae57-ad02c1f40656</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e85093c6-b62d-4703-8289-958dee28943a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0937fd34-67e3-4a09-a916-2d14d276be16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a800c63f-0423-4b78-a2f4-fe1d99388b30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>06d3b504-6fc7-4ecf-ad4d-cad6e8c19b28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85617881-3e24-4ef9-b255-34ae35c25fd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e29252fc-69a2-4a02-a42c-f6bb4e4e7c48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25d2af73-e25e-4f34-adda-cdb5180cfd90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cab7d5f-4f28-4822-870d-40c41ebd5b70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25ae50af-9f3a-456e-aedd-91ec96c0c5dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a4d067df-b2ca-4809-adad-f2e81ada5d67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>36a6c17b-c1cd-4724-bebb-eb77f17108c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68af44b3-63dc-4b97-93a3-37ea6be4304a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d90af029-3a88-4018-83da-514097a6999a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61448afb-c43c-45de-8c57-1b5885463011</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41a0915c-c505-4ed8-83dc-3d0c5d1f5990</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5339da32-8beb-431a-abab-c51992245df9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28c12c70-9539-4a65-8a6c-a66134ea107a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0706000f-78a0-4e14-b68b-599a55b6fb81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3ca6100-03bc-40d8-9fad-aac0aee283de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a46c7d58-61c2-454d-b8b1-68c2edfb8042</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45a9b185-fd8d-4cfc-9112-a5c98f985680</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>168d2243-6656-4b35-a6ec-514bef690a95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28993210-f6ee-4a36-afc1-e4385991755b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d4562725-4d5d-4f84-b1a5-0760bf3ed78e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0445a5f8-0479-4cf8-80cf-2ed980ae1cc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd768d89-9035-46ed-833d-ffbdf861c102</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d6cf387-1fe6-4f02-94b9-1ee8a7d63cd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99174082-76ad-4e0c-b72b-838dd4d1e124</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc9bf0d8-4a4b-4254-a0f0-56e778b4923f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>034d5383-db21-4f46-8411-6cf36bf66355</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9187d7d8-03bc-4a29-831e-11144163dc30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9363f19-5544-4a18-8247-470ee1818e5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a873d74d-0cd7-4153-a419-6865839eb93a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30caf88e-5c6a-4506-adfa-8539a405b16a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>84ce58e9-b77d-48c9-931c-100d1152c370</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15d657e7-3692-4683-bb05-0350762b4ab8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a96e2a5-ca2b-4920-9606-a97b21751d93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06b290b8-6f79-4332-a742-15a499ce69c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aba73db7-f9cb-4559-a597-691fa679c5bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>742e092b-7b24-47bd-94b4-71d3bc68608b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - Permata</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>463a9a3a-0df3-4f25-9068-0b6a54291f53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0003bef-1ff3-443f-a0a4-65ca818a61dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e47e14a-b223-4a75-8a47-c6efea472f8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - Permata</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17226e77-117d-4d7a-badb-20d3788a34b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3528dd53-1361-4554-8817-11cd52f9b8f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53e50b87-ade9-4f1c-84bb-f11ec0649a92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Laksana Mentari PT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d53c5e3c-fe0b-4c1a-9158-32231ab7e1f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31023b34-28d0-4833-8eb1-0c2464f8fab2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22462555-f173-4a14-a2e5-fc37d5255b0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1809e80f-7219-4379-a06f-9873aa9c6d14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - Permata</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>396a4a21-d82f-411b-b1a9-604b530778e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06887d27-d77f-4e5d-a07f-69f254c1feb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA ANR TPS RTGS</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a257a8c7-71d3-457d-a83b-c97b606b92ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>924e4d11-b2f1-4d4b-8290-ef933469dfa7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Giro Valas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55dcec91-58d9-4869-a247-319fcbfb854f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA PT Tahapan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e749129-2faf-480c-a0ef-633240d17df1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
