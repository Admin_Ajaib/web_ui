<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create KYC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a79dcdcc-96c4-4cba-af5a-a4251c5871ea</testSuiteGuid>
   <testCaseLink>
      <guid>eee87499-e942-480b-b158-30db6e40e7cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/1. One Time Request/Registration/Registration - BCA With RT RW</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25954c33-cbcf-408d-8717-6c32d9a81d50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/1. One Time Request/Registration/Registration - BCA With RT RW2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aba27bb4-2f6a-4c5e-af27-7b466d1a6265</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/1. One Time Request/Registration/Registration - BCA Wrong Zip Code2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>317c7d45-27c9-4892-8779-1891c51c00ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/1. One Time Request/Registration/Registration - BCA Wrong Zip Code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9375d7b-dec0-457d-be80-54ef1fb9fa5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Pertamina</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab66d1ad-eae7-4dfd-ae11-a9a44df9f146</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCENARIO/Web/Register/DEV/Registration - BCA Tahapan</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
