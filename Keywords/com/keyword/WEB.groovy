package com.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

import internal.GlobalVariable

public class WEB extends UI {

	@Keyword
	public static void checkLot (String TotalLot, String currentLot, String percent) {
		int result = 0
		int buyLot = currentLot.toInteger()

		if (percent == '25%' || percent == '25 %') {
			def decimal = TotalLot.toInteger() / 4
			result = decimal.toInteger()
		} else if (percent == '50%' || percent == '50 %') {
			def decimal = TotalLot.toInteger() / 2
			result = decimal.toInteger()
		} else if (percent == '75%' || percent == '75 %') {
			def decimal = (TotalLot.toInteger() / 4) * 3
			result = decimal.toInteger()
		} else if (percent == '100%' || percent == '100 %') {
			result = TotalLot.toInteger()
		}

		if (result == buyLot) {
			Note("Lot is match")
		} else {
			KeywordUtil.markWarning("Lot is not Match")
		}
	}

	@Keyword
	public static ArrayList getWatchlist(TestObject tableXpath) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		WebElement table = Driver.findElement(By.xpath(XpathTable))
		List<WebElement> getTable = table.findElements(By.xpath(XpathTable + "/div[2]/div"))

		List<String> collsName = new ArrayList()

		int i
		for (i = 0 ; i < getTable.size() ; i++){
			int count = i + 1
			List<WebElement> Colls = getTable[i].findElements(By.xpath("(" + XpathTable + "/div[2]/div[" + count + "]//span)[1]"))
			if (Colls.size() == 0){
				continue
			} else {
				collsName.add(Colls[0].getText().trim())
			}
		}
		return collsName
	}

	@Keyword
	public static ArrayList getStockList(TestObject tableXpath) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		WebElement table = Driver.findElement(By.xpath(XpathTable))
		List<WebElement> getTable = table.findElements(By.xpath(XpathTable + "//li"))

		List<String> collsName = new ArrayList()

		int i
		for (i = 0 ; i < getTable.size() ; i++){
			int count = i + 1
			List<WebElement> Colls = getTable[i].findElements(By.xpath("(" + XpathTable + "//li[" + count + "]//span)[1]"))
			if (Colls.size() == 0){
				continue
			} else {
				collsName.add(Colls[0].getText().trim())
			}
		}
		return collsName
	}

	@Keyword
	public static ArrayList getTableData(TestObject tableXpath) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		WebElement table = Driver.findElement(By.xpath(XpathTable))
		List<WebElement> getTable = table.findElements(By.tagName("div"))

		List<String> collsName = new ArrayList()

		int i
		for (i = 0 ; i < getTable.size() ; i++){
			int count = i + 1
			List<WebElement> Colls = getTable[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				continue
			} else {
				collsName.add(Colls[0].getText().trim())
			}
		}
		return collsName
	}
}
