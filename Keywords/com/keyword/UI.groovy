package com.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import ch.qos.logback.core.joran.action.Action
import org.openqa.selenium.interactions.Actions
import org.apache.http.client.methods.RequestBuilder
import org.openqa.selenium.remote.RemoteWebElement

import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import java.util.ArrayList
import java.sql.PreparedStatement
import java.sql.Driver
import java.sql.Connection
import java.sql.CallableStatement

import org.openqa.selenium.By as By
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration
import org.apache.commons.io.FileUtils

import java.lang.Object
import java.lang.Process
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.sql.ResultSetMetaData

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.NodeList

import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities
import java.net.InetAddress

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.kms.katalon.core.webui.driver.SmartWaitWebDriver
import com.kms.katalon.core.exception.StepErrorException as StepErrorException

import internal.GlobalVariable

public class UI {
	private static Connection connection = null;

	@Keyword
	public static def connectDB(String database, String environment){
		String IP = null
		String dbname = null
		String password = null
		String connect = null

		if (environment.toLowerCase() == 'dev') {
			if (database.toLowerCase().contains('mysql')) {
				IP = GlobalVariable.mysqldevip
				dbname = GlobalVariable.mysqldevusername
				password = GlobalVariable.mysqldevpass
			} else if (database.toLowerCase().contains('postgre')) {
				IP = GlobalVariable.postgredevip
				dbname = GlobalVariable.postgredevusername
				password = GlobalVariable.postgredevpass
			}
		} else if (environment.toLowerCase() == 'uat') {
			if (database.toLowerCase().contains('mysql')) {
				IP = GlobalVariable.mysqluatip
				dbname = GlobalVariable.mysqluatusername
				password = GlobalVariable.mysqluatpass
			} else if (database.toLowerCase().contains('postgre')) {
				IP = GlobalVariable.postgreuatip
				dbname = GlobalVariable.postgreuatusername
				password = ''
			}
		}

		if (database.toLowerCase().contains('mysql')) {
			connect = "jdbc:mysql://" + IP + "/" + dbname
		} else if (database.toLowerCase().contains('postgre')) {
			connect = "jdbc:postgresql://" + IP + "/" + dbname
		}

		if(connection != null && !connection.isClosed()){
			connection.close()
		}

		connection = DriverManager.getConnection(connect, 'ajaib', password)
		return connection
	}

	@Keyword
	private static void closeDatabaseConnection() {
		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = null
	}

	@Keyword
	private static def executeQuery(String queryString) {
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(queryString)

		return rs
	}

	@Keyword
	private static def execute(String queryString) {
		Statement stm = connection.createStatement()
		boolean result = stm.execute(queryString)

		return result
	}

	@Keyword
	private static int countdbRow (String queryTable) {
		def Query = executeQuery(queryTable)
		ArrayList countRow = new ArrayList()

		while (Query.next()) {
			Object getData = Query.getObject(1)
			countRow.add(getData)
		}

		KeywordUtil.markPassed('Total row is ' + countRow.size())
		return countRow.size()
	}

	@Keyword
	public static String getValueDatabase (String database, String environment, String queryTable, String ColumnName) {
		connectDB(database, environment)
		def Data = executeQuery(queryTable)
		String result

		if (!(Data.next())) {
			String hasil = null

			result = hasil
		} else {
			String getData = Data.getString(ColumnName)
			result = getData
		}
		closeDatabaseConnection()
		return result.toString().trim()
	}

	@Keyword
	public static ArrayList getOneColumnDatabase(String database, String environment, String queryTable, String ColumnName) {
		connectDB(database, environment)
		ArrayList columnData = new ArrayList()

		def Data = executeQuery(queryTable)
		int countRow = countdbRow (queryTable)

		int i
		for (i = 1 ; i <= countRow ; i++) {
			Data.next()
			Object getData = Data.getObject(ColumnName)
			columnData.add(getData.toString().trim())
		}
		closeDatabaseConnection()
		return columnData
	}

	@Keyword
	public static void RunBrowser (String URL) {
		System.setProperty("webdriver.chrome.driver", DriverFactory.getChromeDriverPath())
		ChromeOptions options = new ChromeOptions()
		Map<String, Object> chromePrefs = new HashMap<String, Object>()

		chromePrefs.put("profile.default_content_setting_values.notifications", 1)
		options.addArguments("start-maximized")
		options.addArguments("use-fake-ui-for-media-stream")
		options.setExperimentalOption("prefs", chromePrefs)

		DesiredCapabilities capabilities = new DesiredCapabilities()
		capabilities.setCapability(ChromeOptions.CAPABILITY, options)

		ChromeDriver driver = new ChromeDriver(capabilities)
		driver.get(URL)
		WebDriver changeDriver = DriverFactory.changeWebDriver(driver)
	}

	@Keyword
	public static TestObject newTestObject(String locator){
		TestObject updatedTestObject = new TestObject("Grid")
		updatedTestObject.addProperty("xpath", ConditionType.EQUALS, locator)

		return updatedTestObject
	}

	@Keyword
	public static void HoverItem (TestObject xpath) {
		String toString = xpath.toString()
		String[] separate = toString.split(' ')
		String[] getName = separate[separate.size()-1].split('/')
		String objGet = getName[getName.size()-1]
		String objName = objGet.replace("'", "")

		if (xpath != null || xpath != "") {
			WebUI.mouseOver(xpath, FailureHandling.STOP_ON_FAILURE)
			KeywordUtil.markPassed("Item "+ objName +" has been Hover")
		} else {
			KeywordUtil.markFailedAndStop("Item "+ objName +" is not found, Please recheck your xpath")
		}
	}

	@Keyword
	public static void ClickXpath (String xpath) {
		if (xpath != null || xpath != "") {
			WebUI.waitForElementClickable(newTestObject(xpath), 2)
			WebUI.click(newTestObject(xpath), FailureHandling.STOP_ON_FAILURE)
		} else {
			KeywordUtil.markFailedAndStop("Button is not found, Please recheck your xpath")
		}
	}

	@Keyword
	public static void ClearXpath (String xpath) {
		if (xpath != null || xpath != "") {
			WebUI.clearText(newTestObject(xpath), FailureHandling.STOP_ON_FAILURE)
		} else {
			KeywordUtil.markFailedAndStop("Field is not found, Please recheck your xpath")
		}
	}

	@Keyword
	private static void Click (TestObject xpath) {
		String toString = xpath.toString()
		String[] separate = toString.split(' ')
		String[] getName = separate[separate.size()-1].split('/')
		String objGet = getName[getName.size()-1]
		String objName = objGet.replace("'", "")

		if (xpath != null || xpath != "") {
			WebUI.waitForElementClickable(xpath, 2)
			WebUI.click(xpath, FailureHandling.STOP_ON_FAILURE)
			KeywordUtil.markPassed("Button \'"+ objName +"\' has been Clicked")
		} else {
			KeywordUtil.markFailedAndStop("Button \'"+ objName +"\' is not found, Please recheck your xpath")
		}
	}

	@Keyword
	public static void WriteXpath (String xpath, String text) {
		if (xpath != null || xpath != "") {
			WebUI.setText(newTestObject(xpath), text)
		} else {
			KeywordUtil.markFailedAndStop("Can't write on respected textarea, Please recheck your xpath")
		}
	}

	@Keyword
	public static void ComboBoxOption (TestObject Combo, String Value) {
		TestObject tObj = Combo
		String findXpath = "${tObj.findPropertyValue('xpath')}"
		String children = findXpath + '/option[contains(text(),"' + Value + '")]'

		Click(newTestObject(findXpath))
		Click(newTestObject(children))
	}

	@Keyword
	public static void ComboBoxOptionXpath (String Combo, String Value) {
		String children = Combo + '/option[contains(text(),"' + Value + '")]'

		Click(newTestObject(Combo))
		Click(newTestObject(children))
	}

	@Keyword
	public static void datePicker(String date) {
		String[] separate = date.split(' ')

		String day = separate[0]
		String month = separate[1]
		String year = separate[2]

		String XpathDay = '//*[contains(@class,"week")]/*[not(contains(@class,"outside")) and text()="' + day + '"]'
		String XpathMonth = '//*[contains(@class,"month")]/option[text()="' + month + '"]'
		String XpathYear = '//*[contains(@class,"year")]/option[text()="' + year + '"]'

		Click(newTestObject(XpathMonth))
		Click(newTestObject(XpathYear))
		Click(newTestObject(XpathDay))
	}

	@Keyword
	private static void Sleep(int timeOut) {
		if (timeOut == 0 || timeOut == null || timeOut == "") {
			KeywordUtil.markWarning("Sleep time is unidentified")
		} else {
			WebUI.delay(timeOut)
			KeywordUtil.markPassed("Sleep time is "+ timeOut + " second(s)")
		}
	}

	@Keyword
	public static void UploadFile2 (String pictureName) {
		Sleep(2)
		String locationFile = RunConfiguration.getProjectDir() + '/Plugins/Picture'

		String fileName = RunConfiguration.getProjectDir() + '/Plugins/FileName.txt'
		File write2 = new File(fileName)
		String name = locationFile + "/" + pictureName
		String insertFile = name.replace('/','\\')
		write2.append(insertFile)

		String upload = RunConfiguration.getProjectDir() + '/Plugins/UploadFile2.exe'
		Process runUpload = Runtime.getRuntime().exec(upload)

		KeywordUtil.markPassed('Upload File : ' + pictureName + ' has Finished.' )
		Sleep(3)

		write2.delete()
	}

	@Keyword
	private static String getDateToday (String format) {
		Date date = new Date()
		DateFormat dateFormat = new SimpleDateFormat(format)
		String formattedDate= dateFormat.format(date)
		return formattedDate
	}

	@Keyword // FileName without extension
	public static void ScreenShot (String FileName) {
		try {
			String dateToday = getDateToday('dd-MMMM-HH-mm')
			String[] splitDate = dateToday.split("-")
			String ScreenshotFolder = splitDate[0] + ' ' + splitDate[1] + '/'
			String ScreenshotTime = splitDate[2] + '.' + splitDate[3] + ' - '

			boolean exist = GlobalVariable.metaClass.hasProperty(GlobalVariable, 'count')

			if (!exist) {
				GlobalVar('count', 1)
			}

			Sleep(1)

			String currentFolder = RunConfiguration.getProjectDir() + '/Plugins/Screenshot/' + ScreenshotFolder + GlobalVariable.envi + '/' + GlobalVariable.testcase + '/' + GlobalVariable.count + '. ' + FileName + ".jpeg"
			WebUI.takeScreenshot(currentFolder)
			GlobalVariable.count += 1

		} catch (Exception StepFailedException) {
			KeywordUtil.markWarning("Can't take screenshot")
		}
	}

	@Keyword // use lower case for Global Variable's name
	public static void GlobalVar (String name, def value) {
		boolean exist = GlobalVariable.metaClass.hasProperty(GlobalVariable, name)

		if (!exist) {
			GroovyShell shell1 = new GroovyShell()
			MetaClass mc = shell1.evaluate("internal.GlobalVariable").metaClass
			String getterName = "get" + name.capitalize()
			String setterName = "set" + name.capitalize()
			mc.'static'."$getterName" = { -> return value }
			mc.'static'."$setterName" = { newValue -> value = newValue }
			mc.'static'."$name" = value

		} else {
			if ((GlobalVariable."${name}").getClass().getName() == 'java.util.ArrayList') {

				if (value.getClass().getName() == 'java.util.ArrayList') {
					for (int i = 0 ; i < value.size() ; i++) {
						(GlobalVariable."${name}").add(value[i])
					}
				} else {
					(GlobalVariable."${name}").add(value)
				}
			} else {
				GlobalVariable."${name}" = value
			}
		}
	}

	@Keyword
	public static void Note(def variable) {
		String result = variable.toString()
		WebUI.comment("♥♥♥ " + result + " ♥♥♥")
	}

	@Keyword
	private static void updatePermanently(String envName, String varName, def newValue) {
		File inputFile = new File(RunConfiguration.getProjectDir() + "//Profiles//" + envName + ".glbl")
		if(!Files.exists(Paths.get(inputFile.getAbsolutePath()))) {
			KeywordUtil.markFailed("A file with profile was not found - check path: " + inputFile.getAbsolutePath())
		}

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance()
		DocumentBuilder builder = factory.newDocumentBuilder()
		Document document = builder.parse(inputFile)

		NodeList elems = document.getDocumentElement().getElementsByTagName("GlobalVariableEntity")
		for(Element elem in elems) {
			if(elem.getElementsByTagName("name").item(0).getTextContent() == varName) {
				elem.getElementsByTagName("initValue").item(0).setTextContent(newValue)
				document.getDocumentElement().normalize()
				Transformer transformer = TransformerFactory.newInstance().newTransformer()
				DOMSource source = new DOMSource(document)
				StreamResult result = new StreamResult(inputFile)
				transformer.setOutputProperty(OutputKeys.INDENT, "yes")
				transformer.transform(source, result)
				return
			}
		}
		KeywordUtil.markWarning("Global variable with name " + varName + " was not found.")
	}

	@Keyword
	public static void updateGlobal(String varName, def newValue) {
		if (GlobalVariable."${varName}".getClass().getName() == 'java.lang.Integer') {
			String result = newValue.toString()

			updatePermanently(RunConfiguration.getExecutionProfile(), varName, result)

		} else {
			updatePermanently(RunConfiguration.getExecutionProfile(), varName, newValue)
		}
	}

	@Keyword
	public static void Signature(TestObject Canvas) {
		WebDriver driver = DriverFactory.getWebDriver()
		Actions builder = new Actions(driver)

		WebElement ele = WebUiCommonHelper.findWebElement(Canvas, 20)

		Action drawAction = builder.moveToElement(ele, -240, 50).clickAndHold().moveByOffset(480, -100).build().perform()
		Action drawAction1 = builder.moveToElement(ele, 100, 0).clickAndHold().release().build().perform()

		Action drawAction2 = builder.moveToElement(ele, -240, -50).clickAndHold().moveByOffset(480, 100).build().perform()
		Action drawAction3 = builder.moveToElement(ele, -100, 0).clickAndHold().release().build().perform()
	}

	@Keyword
	public static void forceStop(String comment) {
		throw new StepErrorException(comment)
	}

	@Keyword
	public static int getNumber() {
		boolean tempNomor = GlobalVariable.metaClass.hasProperty(GlobalVariable, 'tempnomor')

		if (!tempNomor) {
			return GlobalVariable.nomor
		} else {
			if (GlobalVariable.nomor > GlobalVariable.tempnomor) {
				return GlobalVariable.nomor
			} else {
				return GlobalVariable.tempnomor
			}
		}
	}

	@Keyword
	public static int getNik() {
		boolean tempNik = GlobalVariable.metaClass.hasProperty(GlobalVariable, 'tempnik')

		if (!tempNik) {
			return GlobalVariable.nik
		} else {
			if (GlobalVariable.nik > GlobalVariable.tempnik) {
				return GlobalVariable.nik
			} else {
				return GlobalVariable.tempnik
			}
		}
	}

	@Keyword
	public static int getSid() {
		boolean tempinc = GlobalVariable.metaClass.hasProperty(GlobalVariable, 'tempinc')

		if (!tempinc) {
			return GlobalVariable.inc
		} else {
			if (GlobalVariable.inc > GlobalVariable.tempinc) {
				return GlobalVariable.inc
			} else {
				return GlobalVariable.tempinc
			}
		}
	}

	@Keyword
	public static int getSre() {
		boolean tempinc = GlobalVariable.metaClass.hasProperty(GlobalVariable, 'tempinc')

		if (!tempinc) {
			return GlobalVariable.inc
		} else {
			if (GlobalVariable.inc > GlobalVariable.tempinc) {
				return GlobalVariable.inc
			} else {
				return GlobalVariable.tempinc
			}
		}
	}

	@Keyword
	public static void killProcess() {
		Runtime.getRuntime().exec("taskkill /im chromedriver.exe /f")
	}

	@Keyword
	public static boolean checkNumber(String text) {
		return text.matches("-?\\d+(\\.\\d+)?")
	}

	@Keyword
	public static boolean checkLetter(String text) {
		return text.matches("[a-zA-Z]+")
	}

	@Keyword
	public static void dragNdrop (TestObject source, TestObject target) {
		WebDriver Driver = DriverFactory.getWebDriver()

		String sObj = "${source.findPropertyValue('xpath')}"
		String tObj = "${target.findPropertyValue('xpath')}"

		WebElement sourceLocator = Driver.findElement(By.xpath("sObj"))
		WebElement targetLocator = Driver.findElement(By.xpath("tObj"))

		Actions action = new Actions(Driver)

		action.dragAndDrop(sourceLocator, targetLocator).build().perform()
	}
}
